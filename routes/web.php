<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Pengiriman
Route::resource('/pengiriman', 'PengirimanController');
Route::post('/pengirimandelete', 'PengirimanController@deletelistpengiriman')->name('pengiriman.deletelistpengiriman');

//PackingList
Route::resource('/packinglist', 'PackingListController');
Route::post('/cetak_pdf','PackingListController@cetak_pdf')->name('packinglist.cetak_pdf');
Route::post('/detailpengirimankapal', 'PackingListController@showdetailpengirimankapal')->name('packing.showdetailpengirimankapal');
Route::post('/packingapproval', 'PackingListController@approval')->name('packing.approval');
Route::post('/fillsealshow', 'PackingListController@fillsealshow')->name('packing.fillsealshow');
Route::post('/submitfillseal', 'PackingListController@submitfillseal')->name('packing.submitfillseal');


//StatusPengiriman
Route::resource('/statuspengiriman', 'StatusPengirimanController');
Route::post('/historipengiriman', 'StatusPengirimanController@showhistoripengiriman')->name('statuspengiriman.showhistoripengiriman');
Route::post('/historicomplete', 'StatusPengirimanController@showcomplete')->name('statuspengiriman.showcomplete');
Route::post('/statuscomplete', 'StatusPengirimanController@statuscomplete')->name('statuspengiriman.statuscomplete');


//klaim
Route::resource('/klaim', 'KlaimPengirimanController');
Route::post('/uploadbrgrusak', 'KlaimPengirimanController@uploadphotoandupdate')->name('klaimpengiriman.uploadphotoandupdate');
Route::post('/uploadkettgl', 'KlaimPengirimanController@uploadketerangantgl')->name('klaimpengiriman.uploadketerangantgl');
Route::post('/updatestatusklaim', 'KlaimPengirimanController@updatestatusklaim')->name('klaim.updatestatusklaim');



//Document
Route::resource('/document', 'DocumentController');


//logout
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

//login
Route::get('loginnew', '\App\Http\Controllers\Auth\LoginController@loginnew')->name('login.loginnew');

//register
route::get("registernew",'\App\Http\Controllers\Auth\RegisterController@registernew')->name('register.registernew');

