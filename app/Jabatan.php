<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Jabatan extends Model
{
	use Notifiable;
    protected $fillable = [
        'nama_jabatan'
    ];

}
