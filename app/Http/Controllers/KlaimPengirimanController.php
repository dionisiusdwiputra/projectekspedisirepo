<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException; 
use Carbon\Carbon;

class KlaimPengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        // show only success and group by sesuai select karena ada beberapa yang dikirim sebagian
         $data["klaimpengiriman"]= DB::select("
            SELECT p.idpengiriman,p.pengirim_idpengirim,p.penerima_idpenerima, b.idbarang, p.kapal, p.tgl_pengiriman, p.nama_container, b.nama_barang, peng.nama_pengirim, pen.nama_penerima, b.qty_barang,b.qty_barang, b.panjang,b.lebar,b.tinggi, b.total_kubikasi, sp.status, p.status_pengiriman_idstatus_pengiriman, bd.upload_foto_brg_rusak, bd.keterangan_barang_rusak, bd.tgl_upload_rusak, bd.keterangan_brg_hilang, bd.tgl_keterangan_hilang, bd.status_klaim_idstatus_klaim, (sk.status) as status_klaim

             FROM pengiriman p, pengiriman_has_barang phb, barang b, pengirim peng, penerima pen, status_pengiriman sp, barang_diterima bd, status_klaim sk

             WHERE bd.pengiriman_idpengiriman = p.idpengiriman and bd.barang_idbarang = b.idbarang and p.pengirim_idpengirim= peng.idpengirim and p.penerima_idpenerima = pen.idpenerima and phb.status_pengiriman_idstatus_pengiriman = sp.idstatus_pengiriman and bd.status_klaim_idstatus_klaim = sk.idstatus_klaim and phb.status_pengiriman_idstatus_pengiriman = 6 
             
             GROUP BY p.idpengiriman,p.pengirim_idpengirim,p.penerima_idpenerima, b.idbarang, p.kapal, p.tgl_pengiriman, p.nama_container, b.nama_barang, peng.nama_pengirim, pen.nama_penerima, b.qty_barang,b.qty_barang, b.panjang,b.lebar,b.tinggi, b.total_kubikasi, sp.status, p.status_pengiriman_idstatus_pengiriman, bd.upload_foto_brg_rusak, bd.tgl_upload_rusak, bd.keterangan_brg_hilang, bd.tgl_keterangan_hilang");
         $data["statusklaim"]= DB::table("status_klaim")->get();
        return view('klaimpengiriman', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function uploadphotoandupdate(Request $request)
    {
        // dd($request->all());
         $this->validate($request, [
                'fotobrgrusak' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
         ]);
        $idpengiriman = $request->get('idpengiriman');
        $tglbrgrusak = date('Y-m-d',strtotime($request->get('tglbarangrusak')));
        $keteranganbrgrusak = $request->get('keteranganbrgrusak');
        $idbarang = $request->get('idbarang');
        $photo = $request->file('fotobrgrusak');

        $fileupload = time()."_".$photo->getClientOriginalName();
 
        // nama folder tempat kemana file diupload
        $tujuan_upload = 'data_file';

        try {
        // proses upload to database
        DB::table('barang_diterima')->where('pengiriman_idpengiriman',$idpengiriman)->where('barang_idbarang',$idbarang)->update(array('upload_foto_brg_rusak'=>$fileupload,'tgl_upload_rusak'=>$tglbrgrusak,'keterangan_barang_rusak'=>$keteranganbrgrusak));
 
        // proses upload file ti direktori
        $photo->move($tujuan_upload, $fileupload);
        
        return redirect('/klaim')->with(['success' => 'Data Berhasil disimpan']); 
        } catch(QueryException $ex){ 
        return redirect('/klaim')->with(['error' => 'Data Gagal Disimpan']); 
        }  
    }
    
     public function uploadketerangantgl(Request $request)
    {
        $idpengiriman = $request->get('idpenghilang');
        $idbarang = $request->get('idbrghilang');
        $ketbrghilang = $request->get('keteranganbrghilang');
        $tglbrghilang = date('Y-m-d',strtotime($request->get('tglbaranghilang')));
        
        try {
        // proses upload to database
        DB::table('barang_diterima')->where('pengiriman_idpengiriman',$idpengiriman)->where('barang_idbarang',$idbarang)->update(array('keterangan_brg_hilang'=>$ketbrghilang,'tgl_keterangan_hilang'=>$tglbrghilang));

        return redirect('/klaim')->with(['success' => 'Data Berhasil disimpan']); 
        } catch (QueryException $e) {
           return redirect('/klaim')->with(['error' => 'Data Gagal Disimpan']); 
        }
        
    }

    public function updatestatusklaim(Request $request)
    {
        $idstatus = $request->get("idstatus");
        $idpeng = $request->get("idpengiriman");
        $idbrg = $request->get("idbarang");


        DB::table('barang_diterima')->where('pengiriman_idpengiriman',$idpeng)->where('barang_idbarang',$idbrg)->update(array('status_klaim_idstatus_klaim'=>$idstatus));


    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
