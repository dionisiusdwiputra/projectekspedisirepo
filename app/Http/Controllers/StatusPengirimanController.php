<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException; 
use Carbon\Carbon;

class StatusPengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data["dropdownstatus"]   = DB::table("status_pengiriman")->get();
        $data["statuspengiriman"] = DB::select("SELECT p.idpengiriman, phb.barang_idbarang, p.pengirim_idpengirim,p.penerima_idpenerima, b.idbarang, p.kapal, p.tgl_pengiriman, p.nama_container, b.nama_barang, peng.nama_pengirim, pen.nama_penerima, b.qty_barang, b.panjang,b.lebar,b.tinggi, b.total_kubikasi, sp.status, phb.status_pengiriman_idstatus_pengiriman, phb.tgl_status_complete, phb.penerima_status_complete from pengiriman p, pengiriman_has_barang phb, barang b, pengirim peng, penerima pen, status_pengiriman sp where p.idpengiriman = phb.pengiriman_idpengiriman and b.idbarang = phb.barang_idbarang and p.pengirim_idpengirim = peng.idpengirim and p.penerima_idpenerima = pen.idpenerima and phb.status_pengiriman_idstatus_pengiriman= sp.idstatus_pengiriman and phb.status_pengiriman_idstatus_pengiriman != 0");
        return view('statuspengiriman',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
            {
            $idpengiriman = $request->get("idpengiriman");
            $idbarang = $request->get("idbarangmodal");
            $tglterima = date('Y-m-d',strtotime($request->get("datetglstatuskirim")));
            $namapenerima = $request->get("namapenerima");
            $notransaksi = $request->get("notransaksi");
            
            //qty database 
            $qtydatabase = $request->get("qtykirim");
            $qtyditerima = $request->get("qtybrg");
            // sum kirim sebagian dari db
            $qtyjumlahbrgdbsisa =$request->get("qtyjumlahbrgdbsisa");
            $qtysisa = $request->get("qtysisa");
            
            // pengurangan setelah kirim sebagian
            // $qtyakhirsebagaian = $request->get("qtyakhirkirimsebagian");
            
            // data sisa di db(hasil pengurangannya (qtykirim-qtysisayangsudahdikirim))
            $qtysisasebagiandb = $request->get("qtybrgkirimsisacheck");

            // pengurangan qty misal dikirim 5 sisa 5, kemudian dikirim total jadi 9 harusnya yang terkirim adalah 9-5=4
            $qtypenguranganqtyterima = $qtyditerima-$qtyjumlahbrgdbsisa;

            try {
            
            // apabila quantity pengiriman tidak sama dengan yang diterima
            if($qtyditerima!=$qtydatabase)
            {
                if($qtyditerima!=null)
                {
                    
                    // insert into db barang_diterima
                    DB::table('barang_diterima')->insert(['pengiriman_idpengiriman'=>$idpengiriman,'barang_idbarang'=>$idbarang,'tgl_terima'=>$tglterima,'nama_penerima'=>$namapenerima,'notransaksi'=>$notransaksi,'qty_barang_diterima'=>$qtypenguranganqtyterima,'qty_sisa'=>$qtysisa,'status_klaim_idstatus_Klaim'=>0]);
                }
                else
                {
                    // insert into db barang_diterima
                    DB::table('barang_diterima')->insert(['pengiriman_idpengiriman'=>$idpengiriman,'barang_idbarang'=>$idbarang,'tgl_terima'=>$tglterima,'nama_penerima'=>$namapenerima,'notransaksi'=>$notransaksi,'qty_barang_diterima'=>$qtyditerima,'qty_sisa'=>$qtysisa,'status_klaim_idstatus_Klaim'=>0]);
                }
            
            // update status menjadi diterima sebagian
            DB::table('pengiriman_has_barang')->where('pengiriman_idpengiriman',$idpengiriman)->where('barang_idbarang',$idbarang)->update(array('status_pengiriman_idstatus_pengiriman'=>"7"));
            
            }
            // hasil akhir dari barang yang sudah diterima sebagian
            else if($qtydatabase!=$qtysisasebagiandb)
            {
            // insert db barang diterima sisanya dari sebagian
            DB::table('barang_diterima')->insert(['pengiriman_idpengiriman'=>$idpengiriman, 'barang_idbarang'=>$idbarang,'tgl_terima'=>$tglterima,'nama_penerima'=>$namapenerima,'notransaksi'=>$notransaksi,'qty_barang_diterima'=>$qtypenguranganqtyterima,'qty_sisa'=>$qtysisa,'status_klaim_idstatus_Klaim'=>0]);
            // update status menjadi selesai atau complete
            // DB::table('pengiriman')->where('idpengiriman',$idpengiriman)->update(array('status_pengiriman_idstatus_pengiriman'=>"6"));
            DB::table('pengiriman_has_barang')->where('pengiriman_idpengiriman',$idpengiriman)->where('barang_idbarang',$idbarang)->update(array('status_pengiriman_idstatus_pengiriman'=>"6"));
            
            }
            // else untuk apabila barang yang diterima sesuai dengan yang dikirim
            else 
            {
            //insert into db barang_diterima
            DB::table('barang_diterima')->insert(['pengiriman_idpengiriman'=>$idpengiriman, 'barang_idbarang'=>$idbarang,'tgl_terima'=>$tglterima,'nama_penerima'=>$namapenerima,'notransaksi'=>$notransaksi,'qty_barang_diterima'=>$qtyditerima,'qty_sisa'=>$qtysisa,'status_klaim_idstatus_Klaim'=>0]);
            // update status menjadi selesai atau complete
            // DB::table('pengiriman')->where('idpengiriman',$idpengiriman)->update(array('status_pengiriman_idstatus_pengiriman'=>"6"));
            DB::table('pengiriman_has_barang')->where('pengiriman_idpengiriman',$idpengiriman)->where('barang_idbarang',$idbarang)->update(array('status_pengiriman_idstatus_pengiriman'=>"6"));
            
            }
            return redirect('/statuspengiriman')->with(['success' => 'Data Berhasil di Update']);
            } catch (QueryException $e) {
            return redirect('/statuspengiriman')->with(['error' => 'Data Gagal di Update']);
            }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showhistoripengiriman(Request $request)
    {
        $idpengiriman = $request->get("idpengiriman");
        $idbarang     = $request->get("idbarang");
        $data         = DB::Select("SELECT b.idbarang, b.nama_barang, bd.tgl_terima,bd.nama_penerima,bd.notransaksi,bd.qty_barang_diterima,bd.qty_sisa from pengiriman_has_barang phb, barang b, barang_diterima bd, pengiriman p where p.idpengiriman = phb.pengiriman_idpengiriman and phb.barang_idbarang=b.idbarang and bd.pengiriman_idpengiriman = p.idpengiriman and bd.barang_idbarang = b.idbarang and bd.pengiriman_idpengiriman = '$idpengiriman' and b.idbarang = '$idbarang'");

        // SELECT b.idbarang, b.nama_barang, bd.tgl_terima,bd.nama_penerima,bd.notransaksi,bd.qty_barang_diterima,bd.qty_sisa from pengiriman_has_barang phb, barang b, barang_diterima bd, pengiriman p where p.idpengiriman = phb.pengiriman_idpengiriman and phb.barang_idbarang=b.idbarang and bd.pengiriman_idpengiriman = p.idpengiriman and bd.pengiriman_idpengiriman = '106' and b.idbarang = '112' 
        return  $data;
    }

    // show khusus status complete
    public function showcomplete(Request $request)
    {
        $idpengiriman = $request->get("idpengirimancomplete");
        $idbarang     = $request->get("idbarangcomplete");
        $data         = DB::Select("SELECT b.idbarang, b.nama_barang, bd.tgl_terima,bd.nama_penerima,bd.notransaksi,bd.qty_barang_diterima,bd.qty_sisa from pengiriman_has_barang phb, barang b, barang_diterima bd, pengiriman p where p.idpengiriman = phb.pengiriman_idpengiriman and phb.barang_idbarang=b.idbarang and bd.pengiriman_idpengiriman = p.idpengiriman and bd.barang_idbarang = b.idbarang and bd.pengiriman_idpengiriman = '$idpengiriman' and b.idbarang = '$idbarang'");
        return  $data;
    }

    public function statuscomplete(Request $request)
    {
        $idpengirimancomplete = $request->get('idpengirimancomplete');
        $idbarangcomplete = $request->get('idbarangcomplete');
        $tgl = date('Y-m-d',strtotime($request->get('tglmodalstatuscomplete')));
        $penerima = $request->get('namapenerimacomplete');
        // dd($request->all());
        try {
            DB::table('pengiriman_has_barang')->where('pengiriman_idpengiriman',$idpengirimancomplete)->where('barang_idbarang',$idbarangcomplete)-> update(['tgl_status_complete'=>$tgl,'penerima_status_complete'=>$penerima]);
         return redirect('/statuspengiriman')->with(['success' => 'Data Status Complete Berhasil Disimpan']); 
            
        } catch (QueryException $ex) {
         return redirect('/statuspengiriman')->with(['error' => 'Data Status Complete Gagal Disimpan']); 
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $idstatuspengiriman = $request->get('idstatuspengiriman');
        $idstatuspengiriman1 = $request->get('idstatuspengiriman1');
        $kapal = $request->get('kapal');
        $container = $request->get('container');
        
        // DB::table('pengiriman')->where('idpengiriman',$id)->update(array('status_pengiriman_idstatus_pengiriman'=>$idstatuspengiriman));
        //DB::table('pengiriman_has_barang')->where('barang_idbarang',$id)->update(array('status_pengiriman_idstatus_pengiriman'=>$idstatuspengiriman));
        // update status sesuai dengan  nama kapal
        // jika status kirim adalah tiba
        if ($idstatuspengiriman1==2) {
            DB::select("update pengiriman_has_barang set pengiriman_has_barang.status_pengiriman_idstatus_pengiriman='$idstatuspengiriman1' where pengiriman_idpengiriman in
                (select p.idpengiriman from pengiriman p where p.kapal = '$kapal')");
        }
        // update status sesuai dengan nama container (gak tau bakal akan ada revisi lagi atau tidak)
        else if ($idstatuspengiriman1==3) {
            DB::select("update pengiriman_has_barang set pengiriman_has_barang.status_pengiriman_idstatus_pengiriman='$idstatuspengiriman1' where pengiriman_idpengiriman in
                (select p.idpengiriman from pengiriman p where p.kapal = '$kapal' and p.nama_container='$container')");
        }
        else
        {
             // update id status selected index
             DB::table('pengiriman_has_barang')->where('barang_idbarang',$id)->update(array('status_pengiriman_idstatus_pengiriman'=>$idstatuspengiriman1));
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
