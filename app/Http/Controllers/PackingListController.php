<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException; 
use App\User;
use Illuminate\Support\Facades\Hash;
use PDF;

class PackingListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data["pengiriman"] = DB::select("SELECT p.idpengiriman, p.kapal, p.tgl_pengiriman, pe.kota_pengirim, pen.kota_penerima, pe.tgl_pengiriman_pengirim, p.status_pengiriman_idstatus_pengiriman, p.status_print, p.keterangan_office, p.keterangan_driver, phb.seal from pengiriman p, pengirim pe, penerima pen , jenis_container jp, pengiriman_has_barang phb, barang b where pe.idpengirim = p.pengirim_idpengirim and pen.idpenerima = p.penerima_idpenerima and p.jenis_container_idcontainer=jp.idcontainer  and p.idpengiriman = phb.pengiriman_idpengiriman and phb.barang_idbarang = b.idbarang group by p.kapal, p.tgl_pengiriman,pe.kota_pengirim,pen.kota_penerima");

        // approval
        $data["user"] = DB::Table("users")->get();

        return view('packing',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function approval(Request $request)
    {
         // dd($request->all());   
        $user =  User::find($request->iduser);
        $pass = $request->pass;
        $idpengiriman = $request->get("idpengiriman");
        $sealapproval = $request->get("sealapproval");
        $namakapal = $request->get("namakapalapproval");
        $keterangandriverapproval = $request->get("keterangandriverapproval");
        $keteranganofficeapproval = $request->get("keteranganofficeapproval");

        // untuk keperluan fill seal
        $idpengirimanfillseal = $request->get("idpengirimanapproval");

        if(Hash::check($pass,$user->password))
        {
            if($sealapproval!=null||$sealapproval!="")
            {
                // update status_print
                foreach($idpengiriman as $id)
                 {
                   $idpengirimanapprove= $id["idpengiriman"];
                   DB::table('pengiriman')->where('idpengiriman',$idpengirimanapprove)->update(array('status_print'=>1));

                 }

                 foreach ($sealapproval as $k)
                {

                    if(isset($k["sealapproval"]))
                    {
                        $seal = $k["sealapproval"];
                        $idbarang = $k["idbarangapproval"];
                        
                        // foreach berdasar id barang
                        DB::table('pengiriman_has_barang')->where('barang_idbarang',$idbarang)->update(array( 'seal'=>$seal));
                    }
                } 
            // update keterangan db untuk approval biar gak input 2x
            DB::table('pengiriman')->where('kapal',$namakapal)->update(array( 'keterangan_office'=>$keteranganofficeapproval,'keterangan_driver'=>$keterangandriverapproval));              
            }
            else
            { 

             // update status_print
            foreach($idpengirimanfillseal as $id)
             {
               $idpengirimanapprove= $id["idpengirimanapproval"];
               DB::table('pengiriman')->where('idpengiriman',$idpengirimanapprove)->update(array('status_print'=>1));

             }
            // update keterangan db untuk approval biar gak input 2x
            DB::table('pengiriman')->where('kapal',$namakapal)->update(array( 'keterangan_office'=>$keteranganofficeapproval,'keterangan_driver'=>$keterangandriverapproval));
            }

            
            
            // benar
            return 1;
        }
        else
        {
            // salah
            return 0;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function cetak_pdf (Request $request)
    {
        // dd($request->all()); 
        $namakapal     = $request->get("namakapaluntukprint");
        $tglkirimprint = $request->get("tglkirimprint");
        $kotapengirim  = $request->get("kotapengirimprint");
        $kotapenerima  = $request->get("kotapenerimaprint");
        
        $idpengiriman  = $request->get("idpengirimanutkpdf");
        $ketoffice     = $request->get("keteranganoffice");
        $ketdriver     = $request->get("keterangandriver");

        // foreach
        $kodeseal = $request->get("kodeseal");

        // update keterangan db
        DB::table('pengiriman')->where('idpengiriman',$idpengiriman)->update(array( 'keterangan_office'=>$ketoffice,'keterangan_driver'=>$ketdriver));

        foreach ($kodeseal as $k)
        {

            if(isset($k["seal"]))
            {
                $seal = $k["seal"];
                $idbarang = $k["idbarang"];
                
                // foreach berdasar id barang
                DB::table('pengiriman_has_barang')->where('barang_idbarang',$idbarang)->update(array( 'seal'=>$seal));
            }

           
        }

        // dd($request->all());
        $data ["tableprint"]= collect(DB::select("SELECT peng.idpengiriman, peng.kapal, p.kota_pengirim, pen.kota_penerima, peng.nama_container, p.nama_pengirim, pen.nama_penerima, peng.tgl_pengiriman, b.nama_barang,b.qty_barang, b.total_kubikasi, jp.nama_jenis_pengiriman,tmp.jenis_container_idcontainer, tmp.sisa_container_temp, peng.jenis_container_idcontainer , peng.keterangan_office, peng.keterangan_driver, phb.seal FROM pengiriman peng, pengirim p, penerima pen, barang b, pengiriman_has_barang phb, jenis_pengiriman jp, jenis_container jk, temp tmp WHERE p.idpengirim = peng.pengirim_idpengirim and pen.idpenerima = peng.penerima_idpenerima and peng.idpengiriman = phb.pengiriman_idpengiriman and peng.idpengiriman = tmp.pengiriman_idpengiriman and b.idbarang = phb.barang_idbarang and jp.idjenis_pengiriman=peng.jenis_pengiriman_idjenis_pengiriman and peng.jenis_container_idcontainer=jk.idcontainer and peng.kapal='$namakapal' and peng.tgl_pengiriman='$tglkirimprint'and p.kota_pengirim='$kotapengirim' and pen.kota_penerima='$kotapenerima'"));

           foreach($data ["tableprint"] as $t)
           {
            // check button disabled
            DB::table('pengiriman')->where('idpengiriman',$t->idpengiriman)->update(array('status_pengiriman_idstatus_pengiriman'=>1,'status_print'=>1));
            // update status per item
            DB::table('pengiriman_has_barang')->where('pengiriman_idpengiriman',$t->idpengiriman)->update(array('status_pengiriman_idstatus_pengiriman'=>1));
           }

         $pdf = PDF::loadview('packing_pdf',$data);
         return $pdf->stream("DaftarIsiPengiriman".$namakapal."".$tglkirimprint.".pdf",array('Attachment'=>true));
    }

     public function showdetailpengirimankapal(Request $request)
    {
         $namakapal = $request->get("namakapal");
         $tglkirim = $request->get("tglkirim");
         $kotapengirim = $request->get("kotapengirim");
         $kotapenerima = $request->get("kotapenerima");

         $data = DB::select("SELECT peng.idpengiriman, peng.nama_container, p.nama_pengirim, pen.nama_penerima, peng.tgl_pengiriman, b.nama_barang, b.idbarang, b.qty_barang, b.total_kubikasi, b.keterangan_tonase_meter, jp.nama_jenis_pengiriman,tmp.jenis_container_idcontainer, tmp.sisa_container_temp, peng.jenis_container_idcontainer,phb.seal, peng.keterangan_office, peng.keterangan_driver FROM pengiriman peng, pengirim p, penerima pen, barang b, pengiriman_has_barang phb, jenis_pengiriman jp, jenis_container jk, temp tmp WHERE p.idpengirim = peng.pengirim_idpengirim and pen.idpenerima = peng.penerima_idpenerima and peng.idpengiriman = phb.pengiriman_idpengiriman and peng.idpengiriman = tmp.pengiriman_idpengiriman and b.idbarang = phb.barang_idbarang and jp.idjenis_pengiriman=peng.jenis_pengiriman_idjenis_pengiriman and peng.jenis_container_idcontainer=jk.idcontainer and peng.tgl_pengiriman=tmp.tgl_pengiriman_temp and peng.kapal='$namakapal' and peng.tgl_pengiriman='$tglkirim' and p.kota_pengirim='$kotapengirim' and pen.kota_penerima='$kotapenerima'");
         return $data;

    }
    public function fillsealshow(Request $request)
    {
         $namakapalseal = $request->get("namakapalfillseal");
         $tglkirimseal = $request->get("tglkirimfillseal");
         $kotapengirimseal = $request->get("kotapengirimfillseal");
         $kotapenerimaseal = $request->get("kotapenerimafillseal");

         $data = DB::select("SELECT peng.idpengiriman, peng.nama_container, peng.kapal, p.nama_pengirim, pen.nama_penerima, peng.tgl_pengiriman, b.nama_barang, b.idbarang, jp.nama_jenis_pengiriman,tmp.jenis_container_idcontainer, tmp.sisa_container_temp, peng.jenis_container_idcontainer,phb.seal, peng.keterangan_office, peng.keterangan_driver FROM pengiriman peng, pengirim p, penerima pen, barang b, pengiriman_has_barang phb, jenis_pengiriman jp, jenis_container jk, temp tmp WHERE p.idpengirim = peng.pengirim_idpengirim and pen.idpenerima = peng.penerima_idpenerima and peng.idpengiriman = phb.pengiriman_idpengiriman and peng.idpengiriman = tmp.pengiriman_idpengiriman and b.idbarang = phb.barang_idbarang and jp.idjenis_pengiriman=peng.jenis_pengiriman_idjenis_pengiriman and peng.jenis_container_idcontainer=jk.idcontainer and peng.tgl_pengiriman=tmp.tgl_pengiriman_temp and peng.kapal='$namakapalseal' and peng.tgl_pengiriman='$tglkirimseal' and p.kota_pengirim='$kotapengirimseal' and pen.kota_penerima='$kotapenerimaseal' group by peng.nama_container");
         return $data;

    }
    public function submitfillseal(Request $request)
    {
        $fillseal = $request->get("fillseal");
        foreach($fillseal as $fs)
        {
            $namakapal = $fs["namakapal"];
            $container = $fs["container"];
            $kodesesal = $fs["inputfillseal"];

            DB::select("update pengiriman_has_barang set pengiriman_has_barang.seal='$kodesesal' where pengiriman_idpengiriman in
                (select p.idpengiriman from pengiriman p where p.kapal = '$namakapal' and p.nama_container = '$container')");

            // DB::table('pengiriman')->where('kapal',$namakapal)->update(array( 'keterangan_office'=>$keteranganofficeapproval,'keterangan_driver'=>$keterangandriverapproval)); 
        }
        return redirect('/packinglist')->with(['success' => 'Data Seal Berhasil disimpan']); 

    }
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
