<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException; 
use Carbon\Carbon;

class PengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data["listcontainer"] = DB::table("jenis_container")->get();
        $data["listjenispengiriman"] = DB::table("jenis_pengiriman")->get();
        $data["listpengirim"] = DB::Table("pengirim")->groupBy('pengirim.nama_pengirim')->get();
        $data["listpenerima"] = DB::Table("penerima")->get();
        // show only data pengriiman yang belum di proses/packinglist
        $data["pengiriman"] =DB::Select("SELECT p.idpengiriman,p.pengirim_idpengirim,p.penerima_idpenerima, b.idbarang, p.kapal, p.tgl_pengiriman, p.nama_container, b.nama_barang, peng.nama_pengirim, pen.nama_penerima, b.qty_barang,b.qty_barang, b.panjang,b.lebar,b.tinggi, b.total_kubikasi, t.sisa_container_temp from pengiriman p, pengiriman_has_barang phb, barang b, pengirim peng, penerima pen, temp t  where p.idpengiriman = phb.pengiriman_idpengiriman and b.idbarang = phb.barang_idbarang and p.pengirim_idpengirim = peng.idpengirim and p.penerima_idpenerima = pen.idpenerima and p.idpengiriman=t.pengiriman_idpengiriman and peng.idpengirim = t.pengirim_idpengirim and pen.idpenerima =  t.penerima_idpenerima and phb.status_pengiriman_idstatus_pengiriman = 0 and p.status_print = 0");
        // $data["kontainerkapal"] = DB::Table("temp")->get();
        $data["kontainerkapal"] = DB::Table("temp")
        ->join('jenis_container','temp.jenis_container_idcontainer','=','jenis_container.idcontainer')
        ->select(DB::raw('temp.*, jenis_container.batas_minimum, min(temp.sisa_container_temp) as sisa_container_temp'))
        ->groupBy('temp.nama_container_temp')
        ->get();
        $data["namakapaltemp"] = DB::Select("SELECT t.kapal_temp, t.tgl_pengiriman_temp from temp t GROUP BY t.kapal_temp");
        return view ('pengiriman',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {  
       // dd($request->all());
       // pengirim
       $idpengirim = $request->get("idpengirim");
       $namapengirim = $request->get("namapengiriminsert");
       $alamatpengirim = $request->get("alamatpengiriminsert");
       $kotapengirim =$request->get("kotapengiriminsert");
       $notelppengirim =$request->get("notelppengiriminsert");


       // penerima
       $idpenerima = $request->get("idpenerima");
       $namapenerima = $request->get("namapenerimainsert");
       $alamatpenerima = $request->get("alamatpenerimainsert");
       $kotapenerima =$request->get("kotapenerimainsert");
       $notelppenerima =$request->get("notelppenerimainsert");

       $namakapal = $request->get("namakapal");
       $tglkeberangkatan = date('Y-m-d',strtotime($request->get("datetglkapal")));

       // id jenis container dari jenis_container
       $idjeniscontainer =$request->get("inputcontainer");

       // id jenis container dari `
       $idjenisconttemp =$request->get("idjenisconttemp");
       
       $namacontainer =$request->get("namacontainer");

       $jenispengiriman =$request->get('inputpengiriman');
       $barang = $request->get('barang');

       $datenow = Carbon::now();
       $sisacontainer = $request->get("sisatampilupdatedb");

       // dd($request->all());

       //update data jenis container data minim
       // DB::Table("jenis_container")->where('idcontainer',$idjeniscontainer)->update(array("update"=>$sisacontainer));

        
            // jika pengirim dan penerima baru
            if($idpengirim==0 && $idpenerima==0)
            {
                 //Data Pengirim Baru
                 $getidpengirim00=DB::table('pengirim')->insertGetId(['nama_pengirim'=>$namapengirim,'alamat_pengirim'=>$alamatpengirim,'kota_pengirim'=>$kotapengirim,'no_telp_pengirim'=>$notelppengirim,'tgl_pengiriman_pengirim'=>$tglkeberangkatan, 'created_at'=>$datenow]);
                 //Data Penerima Baru
                 $getidpenerima00= DB::table('penerima')->insertGetId(['nama_penerima'=>$namapenerima,'alamat_penerima'=>$alamatpenerima,'kota_penerima'=>$kotapenerima,'no_telp_penerima'=>$notelppenerima, 'created_at'=>$datenow]);

                 if ($idjeniscontainer!=null) 
                 {
                    $idpengiriman =  DB::table('pengiriman')
                    ->insertGetId(
                    ['pengirim_idpengirim' => $getidpengirim00 , 
                    'penerima_idpenerima' => $getidpenerima00,
                    'jenis_container_idcontainer'=>$idjeniscontainer,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman'=>$tglkeberangkatan,
                    'kapal'=>$namakapal,
                    'nama_container'=>$namacontainer,
                    'status_pengiriman_idstatus_pengiriman'=>0]
                     );

                    // temp
                     DB::table('temp') ->insert(
                    ['pengiriman_idpengiriman' => $idpengiriman ,
                    'pengirim_idpengirim' => $getidpengirim00 , 
                    'penerima_idpenerima' => $getidpenerima00,
                    'jenis_container_idcontainer'=>$idjeniscontainer,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman_temp'=>$tglkeberangkatan,
                    'kapal_temp'=>$namakapal,
                    'nama_container_temp'=>$namacontainer,
                    'sisa_container_temp'=>$sisacontainer]
                     );

                 }
                // else untuk apabila memilih jenis container yang ada di temp/db
                 else {  
                    $idpengiriman =  DB::table('pengiriman')
                    ->insertGetId(
                    ['pengirim_idpengirim' => $getidpengirim00 , 
                    'penerima_idpenerima' => $getidpenerima00,
                    'jenis_container_idcontainer'=>$idjenisconttemp,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman'=>$tglkeberangkatan,
                    'kapal'=>$namakapal,
                    'nama_container'=>$namacontainer,
                    'status_pengiriman_idstatus_pengiriman'=>0]
                     );

                    // temp
                     DB::table('temp') ->insert(
                    ['pengiriman_idpengiriman' => $idpengiriman ,
                    'pengirim_idpengirim' => $getidpengirim00 , 
                    'penerima_idpenerima' => $getidpenerima00,
                    'jenis_container_idcontainer'=>$idjenisconttemp,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman_temp'=>$tglkeberangkatan,
                    'kapal_temp'=>$namakapal,
                    'nama_container_temp'=>$namacontainer,
                    'sisa_container_temp'=>$sisacontainer]
                     );

                     // update value sisa container menjadi sisa container terakhir sesuai dengan nama container yang sama
                     DB::Table("temp")->where("nama_container_temp",$namacontainer)->update(array("sisa_container_temp"=>$sisacontainer));
                 }


            }
            // jika pengirim sudah ada di db dan penerima baru
            else if($idpengirim!=0 && $idpenerima==0)
            {
                $getidpenerima10= DB::table('penerima')->insertGetId(['nama_penerima'=>$namapenerima,'alamat_penerima'=>$alamatpenerima,'kota_penerima'=>$kotapenerima,'no_telp_penerima'=>$notelppenerima]);

                // update tanggal pengirim pengiriman
                // DB::table('pengirim')->where('idpengirim',$idpengirim)->update(array('tgl_pengiriman_pengirim'=>$tglkeberangkatan));
                 if ($idjeniscontainer!=null) 
                 {
                    $idpengiriman =  DB::table('pengiriman')
                    ->insertGetId(
                    ['pengirim_idpengirim' => $idpengirim , 
                    'penerima_idpenerima' => $getidpenerima10,
                    'jenis_container_idcontainer'=>$idjeniscontainer,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman'=>$tglkeberangkatan,
                    'kapal'=>$namakapal,
                    'nama_container'=>$namacontainer,
                    'status_pengiriman_idstatus_pengiriman'=>0]
                     );

                    // temp
                     DB::table('temp') ->insert(
                    ['pengiriman_idpengiriman' => $idpengiriman ,
                    'pengirim_idpengirim' => $idpengirim, 
                    'penerima_idpenerima' => $getidpenerima10,
                    'jenis_container_idcontainer'=>$idjeniscontainer,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman_temp'=>$tglkeberangkatan,
                    'kapal_temp'=>$namakapal,
                    'nama_container_temp'=>$namacontainer,
                    'sisa_container_temp'=>$sisacontainer]
                     );
                  }
                  // else untuk apabila memilih jenis container yang ada di temp/db
                 else{
                    $idpengiriman =  DB::table('pengiriman')
                    ->insertGetId(
                    ['pengirim_idpengirim' => $idpengirim , 
                    'penerima_idpenerima' => $getidpenerima10,
                    'jenis_container_idcontainer'=>$idjenisconttemp,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman'=>$tglkeberangkatan,
                    'kapal'=>$namakapal,
                    'nama_container'=>$namacontainer,
                    'status_pengiriman_idstatus_pengiriman'=>0]
                     );

                    // temp
                     DB::table('temp') ->insert(
                    ['pengiriman_idpengiriman' => $idpengiriman ,
                    'pengirim_idpengirim' => $idpengirim , 
                    'penerima_idpenerima' => $getidpenerima10,
                    'jenis_container_idcontainer'=>$idjenisconttemp,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman_temp'=>$tglkeberangkatan,
                    'kapal_temp'=>$namakapal,
                    'nama_container_temp'=>$namacontainer,
                    'sisa_container_temp'=>$sisacontainer]
                     );

                     // update value sisa container menjadi sisa container terakhir sesuai dengan nama container yang sama
                     DB::Table("temp")->where("nama_container_temp",$namacontainer)->update(array("sisa_container_temp"=>$sisacontainer));
                 }

            }
            // jika penerima sudah ada di db dan pengirim baru
            else if($idpengirim==0 && $idpenerima!=0)
            {
                //insert Pengirim dan tanggal pengirim
                $getidpengirim01=DB::table('pengirim')->insertGetId(['nama_pengirim'=>$namapengirim,'alamat_pengirim'=>$alamatpengirim,'kota_pengirim'=>$kotapengirim,'no_telp_pengirim'=>$notelppengirim,'tgl_pengiriman_pengirim'=>$tglkeberangkatan]);

                if ($idjeniscontainer!=null) 
                 {
                    $idpengiriman =  DB::table('pengiriman')
                    ->insertGetId(
                    ['pengirim_idpengirim' => $getidpengirim01 , 
                    'penerima_idpenerima' => $idpenerima,
                    'jenis_container_idcontainer'=>$idjeniscontainer,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman'=>$tglkeberangkatan,
                    'kapal'=>$namakapal,
                    'nama_container'=>$namacontainer,
                    'status_pengiriman_idstatus_pengiriman'=>0]
                     );

                    // temp
                     DB::table('temp') ->insert(
                    ['pengiriman_idpengiriman' => $idpengiriman ,
                    'pengirim_idpengirim' => $getidpengirim01 , 
                    'penerima_idpenerima' => $idpenerima,
                    'jenis_container_idcontainer'=>$idjeniscontainer,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman_temp'=>$tglkeberangkatan,
                    'kapal_temp'=>$namakapal,
                    'nama_container_temp'=>$namacontainer,
                    'sisa_container_temp'=>$sisacontainer]
                     );
                 }
                 // else untuk apabila memilih jenis container yang ada di temp/db
                else {
                    $idpengiriman =  DB::table('pengiriman')
                    ->insertGetId(
                    ['pengirim_idpengirim' => $getidpengirim01 , 
                    'penerima_idpenerima' => $idpenerima,
                    'jenis_container_idcontainer'=>$idjenisconttemp,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman'=>$tglkeberangkatan,
                    'kapal'=>$namakapal,
                    'nama_container'=>$namacontainer,
                    'status_pengiriman_idstatus_pengiriman'=>0]
                     );

                    // temp
                     DB::table('temp') ->insert(
                    ['pengiriman_idpengiriman' => $idpengiriman ,
                    'pengirim_idpengirim' => $getidpengirim01 , 
                    'penerima_idpenerima' => $idpenerima,
                    'jenis_container_idcontainer'=>$idjenisconttemp,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman_temp'=>$tglkeberangkatan,
                    'kapal_temp'=>$namakapal,
                    'nama_container_temp'=>$namacontainer,
                    'sisa_container_temp'=>$sisacontainer]
                     );

                     // update value sisa container menjadi sisa container terakhir sesuai dengan nama container yang sama
                     DB::Table("temp")->where("nama_container_temp",$namacontainer)->update(array("sisa_container_temp"=>$sisacontainer));
                }
            }
            else
            {
                // update tgl pengiriman pengirim
                // DB::table('pengirim')->where('idpengirim',$idpengirim)->update(array('tgl_pengiriman_pengirim'=>$tglkeberangkatan));
                 if ($idjeniscontainer!=null) 
                 {
                    $idpengiriman =  DB::table('pengiriman')
                    ->insertGetId(
                    ['pengirim_idpengirim' => $idpengirim , 
                    'penerima_idpenerima' => $idpenerima,
                    'jenis_container_idcontainer'=>$idjeniscontainer,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman'=>$tglkeberangkatan,
                    'kapal'=>$namakapal,
                    'nama_container'=>$namacontainer,
                    'status_pengiriman_idstatus_pengiriman'=>0]
                     );

                    // temp
                     DB::table('temp') ->insert(
                    ['pengiriman_idpengiriman' => $idpengiriman ,
                    'pengirim_idpengirim' => $idpengirim , 
                    'penerima_idpenerima' => $idpenerima,
                    'jenis_container_idcontainer'=>$idjeniscontainer,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman_temp'=>$tglkeberangkatan,
                    'kapal_temp'=>$namakapal,
                    'nama_container_temp'=>$namacontainer,
                    'sisa_container_temp'=>$sisacontainer]
                     );
                 }
                 // else untuk apabila memilih jenis container yang ada di temp/db
                 else{
                    $idpengiriman =  DB::table('pengiriman')
                    ->insertGetId(
                    ['pengirim_idpengirim' => $idpengirim , 
                    'penerima_idpenerima' => $idpenerima,
                    'jenis_container_idcontainer'=>$idjenisconttemp,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman'=>$tglkeberangkatan,
                    'kapal'=>$namakapal,
                    'nama_container'=>$namacontainer,
                    'status_pengiriman_idstatus_pengiriman'=>0]
                     );

                    // temp
                     DB::table('temp') ->insert(
                    ['pengiriman_idpengiriman' => $idpengiriman ,
                    'pengirim_idpengirim' => $idpengirim , 
                    'penerima_idpenerima' => $idpenerima,
                    'jenis_container_idcontainer'=>$idjenisconttemp,
                    'jenis_pengiriman_idjenis_pengiriman'=>$jenispengiriman,
                    'tgl_pengiriman_temp'=>$tglkeberangkatan,
                    'kapal_temp'=>$namakapal,
                    'nama_container_temp'=>$namacontainer,
                    'sisa_container_temp'=>$sisacontainer]
                     );

                     // update value sisa container menjadi sisa container terakhir sesuai dengan nama container yang sama
                     DB::Table("temp")->where("nama_container_temp",$namacontainer)->update(array("sisa_container_temp"=>$sisacontainer));
                 }
            }
      
      try {
        
            foreach($barang as $b)
            {   
            $namabarang = $b['namabarang'];
            $qty = $b['qty'];
            $tonasemeter = $b['tonasemeter'];
            $panjang = $b['panjang'];
            $lebar = $b['lebar'];
            $tinggi = $b['tinggi'];
            $hasil = $b['hasil'];
            $idbarang=DB::table('barang')->insertGetId(
                ['nama_barang' => $namabarang , 'qty_barang' => $qty,'panjang'=>$panjang,'lebar'=>$lebar,'tinggi'=>$tinggi,'total_kubikasi'=>$hasil, 'keterangan_tonase_meter'=>$tonasemeter]
            ); 
                // pengirim dan penerima baru
                 if($idpengirim==0 && $idpenerima==0)
                {
                   //table pengirimans_has_barang
                   DB::table('pengiriman_has_barang')->insert(['pengiriman_idpengiriman'=>$idpengiriman,'pengiriman_pengirim_idpengirim'=>$getidpengirim00,'pengiriman_penerima_idpenerima'=>$getidpenerima00,'barang_idbarang'=>$idbarang]);
                }
                // jika pengirim sudah ada di db dan penerima baru
                else if($idpengirim!=0 && $idpenerima==0)
                {
                    //table pengirimans_has_barang
                   DB::table('pengiriman_has_barang')->insert(['pengiriman_idpengiriman'=>$idpengiriman,'pengiriman_pengirim_idpengirim'=>$idpengirim,'pengiriman_penerima_idpenerima'=>$getidpenerima10,'barang_idbarang'=>$idbarang]);
                }
                // jika penerima sudah ada di db dan pengirim baru
                else if($idpengirim==0 && $idpenerima!=0)
                {
                    //table pengirimans_has_barang
                   DB::table('pengiriman_has_barang')->insert(['pengiriman_idpengiriman'=>$idpengiriman,'pengiriman_pengirim_idpengirim'=>$getidpengirim01,'pengiriman_penerima_idpenerima'=>$idpenerima,'barang_idbarang'=>$idbarang]);
                }   
                // pengirim dan penerima sudah ada di db
                else
                {
                    //table pengirimans_has_barang
                   DB::table('pengiriman_has_barang')->insert(['pengiriman_idpengiriman'=>$idpengiriman,'pengiriman_pengirim_idpengirim'=>$idpengirim,'pengiriman_penerima_idpenerima'=>$idpenerima,'barang_idbarang'=>$idbarang]);
                }
                
             }
            return redirect('/pengiriman')->with(['success' => 'Data Pengiriman Berhasil disimpan']); 
            } catch(QueryException $ex){ 
            return redirect('/pengiriman')->with(['error' => 'Data Pengiriman Gagal disimpan']); 
            }  
    }
     public function prosespengiriman()
    {

      
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $idpengiriman = $request->get("idpengirimanupdate");
        $idbarang = $request->get("idbarangupdate");
        $idpengirim = $request->get("idpengirimupdate");
        $idpenerima = $request->get("idpenerimaupdate");
        $kapal = $request->get("kapalupdate");
        $tgl = date('Y-m-d', strtotime($request->get("datetgleditkapalupdate")));
        $container = $request->get("containerupdate");
        $pengirim = $request->get("pengirimupdate");
        $penerima = $request->get("penerimaupdate");
        $barang = $request->get("barangupdate");
        $qty = $request->get("qtyupdate");
        $panjang = $request->get("panjangupdate");
        $lebar = $request->get("lebarupdate");
        $tinggi = $request->get("tinggiupdate");
        $totalkubikasi = $request->get("totalkubikasiupdate");
        $sisatampiledit = $request->get("sisatampiledit");
        // dd($request->all());
        try {

        DB::table('pengiriman')->where('idpengiriman',$id)->update(array('tgl_pengiriman'=>$tgl,'kapal'=>$kapal,'nama_container'=>$container));
        DB::table('temp')->where('pengiriman_idpengiriman',$id)->update(array('tgl_pengiriman_temp'=>$tgl,'kapal_temp'=>$kapal,'nama_container_temp'=>$container));
        DB::Table("temp")->where("nama_container_temp",$container)->update(array("sisa_container_temp"=>$sisatampiledit));// update value sisa container menjadi sisa container terakhir sesuai dengan nama container yang sama
        DB::table('pengirim')->where('idpengirim',$idpengirim)->update(array('nama_pengirim'=>$pengirim));
        DB::table('penerima')->where('idpenerima',$idpenerima)->update(array('nama_penerima'=>$penerima));
        DB::table('barang')->where('idbarang',$idbarang)->update(array('nama_barang'=>$barang,'qty_barang'=>$qty,'panjang'=>$panjang,'lebar'=>$lebar,'tinggi'=>$tinggi,'total_kubikasi'=>$totalkubikasi));
       return redirect('/pengiriman')->with(['success' => 'Data Pengiriman Berhasil Di Update']); 
        } catch(QueryException $ex){ 
        return redirect('/pengiriman')->with(['error' => 'Data Pengiriman Gagal Di Update']); 
        }  

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    public function deletelistpengiriman(Request $request)
    {
        $idpengiriman = $request->get("idpengirimandeletehide");
        $idbarang = $request->get("idbarangdeletehide");
        $sisacontainerafterdelete = $request->get("sisacontainertemp");
        $containerdelete = $request->get("namacontaineruntukdelete");
        // $idpengirim = $request->get("idpengirimdeletehide");
        // $idpenerima = $request->get("idpenerimadeletehide"); 



        DB::table('temp')->where('nama_container_temp',$containerdelete)->update(array('sisa_container_temp'=>$sisacontainerafterdelete));
        // delete all pengiriman dengan id pengiriman tertentu karena apabila diubah satu persatu dapat mengacaukan total kubikasi nantinya
        DB::table('pengiriman_has_barang')->where('pengiriman_idpengiriman', $idpengiriman)->where('barang_idbarang', $idbarang)->delete();
        DB::table('barang')->where('idbarang', $idbarang)->delete();



        // DB::table('pengiriman')->where('idpengiriman', $idpengiriman)->delete();
        // DB::table('temp')->where('pengiriman_idpengiriman', $idpengiriman)->delete();
        
        

    }
}
