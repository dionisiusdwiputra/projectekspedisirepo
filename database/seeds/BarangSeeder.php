<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 1; $i <= 50; $i++){
    	    // insert data ke table pegawai menggunakan Faker
    	    $qty_barang =  $faker->numberBetween(1,3);
    	    $panjang = $faker->numberBetween(1,3);
    	    $lebar = $faker->numberBetween(1,3);
    	    $tinggi = $faker->numberBetween(1,3);
    	    $total = $qty_barang*$panjang*$lebar*$tinggi;
    		DB::table('barang')->insert([
    			'nama_barang' =>$faker->randomElement(['Tas','Baju','Ayam Beku','Mainan', 'Sepatu']),
    			'qty_barang' => $qty_barang,
    			'panjang' => $panjang,
    			'lebar' => $lebar,
    			'tinggi' => $tinggi,
    			'total_kubikasi' => $total
    		]);
    	}
    }
}
