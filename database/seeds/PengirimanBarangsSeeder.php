<?php

use Illuminate\Database\Seeder;

class PengirimanBarangsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	  
        for($i = 1; $i <= 50; $i++)
        {
    	    // insert data ke table pegawai menggunakan Faker
    	    
   			$ids= 0;
	        $idpengirims = 0;
	        $idpenerimas = 0;
	        $idbarangs= 0;


   			$id= $ids+$i;
	        $idpengirim = $idpengirims+$i;
	        $idpenerima = $idpenerimas+$i;
	        $idbarang= $idbarangs+$i;	
    		DB::table('pengiriman_has_barang')->insert([
    			'pengiriman_idpengiriman' =>$id,
    			'pengiriman_pengirim_idpengirim' => $idpengirim,
    			'pengiriman_penerima_idpenerima' => $idpenerima,
    			'barang_idbarang' => $idbarang
    		]);
    	}
    }
}
