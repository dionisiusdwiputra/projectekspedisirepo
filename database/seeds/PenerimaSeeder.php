<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PenerimaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 1; $i <= 50; $i++){
 
    	      // insert data ke table pegawai menggunakan Faker
    		DB::table('penerima')->insert([
    			'nama_penerima' => $faker->name,
    			'alamat_penerima' => $faker->address,
    			'kota_penerima' => 'Semarang',
    			'no_telp_penerima' => $faker->e164PhoneNumber
    		]);
    	}
    }
}
