<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PengirimSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 1; $i <= 50; $i++){
			$tanggal = $faker->dateTimeBetween('2020-10-14','2020-10-18')->format('Y-m-d');
    	      // insert data ke table pegawai menggunakan Faker
    		$idpengirim = DB::table('pengirim')->insertGetId([
    			'nama_pengirim' => $faker->name,
    			'alamat_pengirim' => $faker->address,
    			'kota_pengirim' => 'Surabaya',
    			'no_telp_pengirim' => $faker->e164PhoneNumber,
    			'tgl_pengiriman_pengirim'=>$tanggal
    		]);
			DB::table('pengiriman')->insert([
				'pengirim_idpengirim' => $idpengirim,
				'penerima_idpenerima' => $faker->unique(true)->numberBetween(1,50),
				'jenis_container_idcontainer' => $faker->unique(true)->numberBetween(7,11),
				'jenis_pengiriman_idjenis_pengiriman' => $faker->unique(true)->numberBetween(1,4),
				'tgl_pengiriman' => $tanggal,
				'kapal' =>$faker->randomElement(['Jaya 1','Keramba 1','Lumba Laut','Radja Lautan 1', 'Queen sea 2']),
				'nama_container' =>$faker->randomElement(['Teru 333','Teru 213124','Teru 7748','Teru 32149', 'Teru 68867'])	
			]);
 
    	}
    }
}
