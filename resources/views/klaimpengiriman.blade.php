@extends('layouts.layout')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Klaim Pengiriman
        <!-- <small>Control panel</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Klaim Pengiriman</li>
      </ol>
     </section>
      <section class ="content">
      <div class="row">
        <div class="col-xs-12">
        @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong>{{ $message }}</strong>
          </div>
        @endif

        @if ($message = Session::get('error'))
          <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
        @endif

        @if ($message = Session::get('warning'))
          <div class="alert alert-warning alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
        @endif

        @if ($message = Session::get('info'))
          <div class="alert alert-info alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
        @endif

        @if ($errors->any())
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            Please check the form below for errors
        </div>
        @endif
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Klaim Pengiriman</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="klaimdatatable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Kapal</th>
                  <th>Tanggal Trip</th>
                  <th>Container</th>
                  <th>Barang</th>
                  <th>Qty(Coly)</th>
                  <th>Pengirim</th>
                  <th>Penerima</th>
                  <th>Action</th>
                  <th>Status Klaim</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                      for ($i=0; $i<count($klaimpengiriman); $i++) 
                      { 
                          $klaim=$klaimpengiriman[$i];
                        ?>   
                        <tr>
                          <td>
                             <?php 
                                 echo $klaim->kapal;
                              ?>
                          </td>
                          <td>
                             <?php 
                                 echo $klaim->tgl_pengiriman;
                              ?>
                          </td>
                          <td>
                             <?php 
                                 echo $klaim->nama_container;
                              ?>
                          </td>
                          <td>
                             <?php 
                                 echo $klaim->nama_barang;
                              ?>
                          </td>
                          <td>
                             <?php 
                                 echo $klaim->qty_barang;
                              ?>
                          </td>
                          <td>
                             <?php 
                                 echo $klaim->nama_pengirim;
                              ?>
                          </td>
                          <td>
                             <?php 
                                 echo $klaim->nama_penerima;
                              ?>
                          </td>
                          <td>  
                          <?php 
                          if ($klaim->upload_foto_brg_rusak == null && $klaim->tgl_upload_rusak == null && $klaim->keterangan_brg_hilang ==null && $klaim->tgl_keterangan_hilang == null) {
                            ?>
                            <button type="button" class="btn btn-warning barangrusak" id="updatebarangrusak" data-toggle="modal" data-target="#barangrusak" data-idpengiriman = "{{$klaim->idpengiriman}}" data-idbarang="{{$klaim->idbarang}}"> Barang Rusak &nbsp; <div class="fa fa-chain-broken"></div></button>

                              <button type="button" class="btn btn-danger baranghilang" data-toggle="modal" data-target="#baranghilang" data-idpengiriman = "{{$klaim->idpengiriman}}" data-idbarang="{{$klaim->idbarang}}">Barang Hilang &nbsp; <div class="fa fa-close"></div></button>
                            
                            <?php 

                            // untuk view gambar barang rusak
                          } else if ($klaim->upload_foto_brg_rusak != null && $klaim->tgl_upload_rusak != null && $klaim->keterangan_brg_hilang ==null && $klaim->tgl_keterangan_hilang == null)
                          {
                           ?>              
                               <button type="button" class="btn btn-warning btnviewbrgrusak" 
                               data-fotobrgrusak = "{{$klaim->upload_foto_brg_rusak}}" 
                               data-tglbrgrusak = "{{$klaim->tgl_upload_rusak}}" 
                               data-keteranganbrgrusak = "{{$klaim->keterangan_barang_rusak}}"
                               style="display: block; margin: auto;">Barang Rusak &nbsp; <div class="fa fa-close"></div></button>

                            <?php   

                            // untuk view barang hilang 
                          } else{
                            ?>
                               <button type="button" class="btn btn-danger btnviewbrghilang" 
                               data-ketbrghilang = "{{$klaim->keterangan_brg_hilang}}" 
                               data-tglbrghilang = "{{$klaim->tgl_keterangan_hilang}}" 
                               style="display: block; margin: auto;">Barang Hilang &nbsp; <div class="fa fa-close"></div></button>
                            <?php  
                          }
                            ?>
                          </td>
                          <!-- STATUS KLAIM -->
                          <td style="text-align: center; vertical-align: middle;">
                          <!-- Status Klaim Done -->
                          <?php if ($klaim->status_klaim_idstatus_klaim==1) {?>
                            <div class="btn-group">
                              <button type="button" class="btn btn-success"><?php echo $klaim->status_klaim ?></button>
                              <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <?php 
                                $first = true;
                                 foreach($statusklaim as $sts)
                                  {
                                    if ($first) { $first=false; continue; }
                                   ?>
                                    <li><a class="idstatusklaim" 
                                      data-idstatusklaim="{{$sts->idstatus_klaim}}"
                                      data-idpengiriman = "{{$klaim->idpengiriman}}" 
                                      data-idbarang="{{$klaim->idbarang}}"
                                      href="#">{{$sts->status}}</a></li>
                                   <?php 
                                  } 
                                ?>
                              </ul>
                            </div>
                            <!-- Status Klaim Progress -->
                          <?php } else if($klaim->status_klaim_idstatus_klaim==2) { ?>
                            <div class="btn-group">
                              <button type="button" class="btn bg-navy"><?php echo $klaim->status_klaim ?></button>
                              <button type="button" class="btn bg-navy dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <?php 
                                 $first = true;
                                 foreach($statusklaim as $sts)
                                  {
                                    if ($first) { $first=false; continue; }
                                   ?>
                                    <li><a class="idstatusklaim" 
                                      data-idstatusklaim="{{$sts->idstatus_klaim}}"
                                      data-idpengiriman = "{{$klaim->idpengiriman}}" 
                                      data-idbarang="{{$klaim->idbarang}}"
                                      href="#">{{$sts->status}}</a></li>
                                   <?php 
                                  } 
                                ?>
                              </ul>
                            </div>
                          <?php } else { ?>
                            <div class="btn-group">
                              <button type="button" class="btn bg-purple">Status Klaim</button>
                              <button type="button" class="btn bg-purple dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <?php 
                                 $first = true;
                                 foreach($statusklaim as $sts)
                                  {
                                    if ($first) { $first=false; continue; }
                                   ?>
                                    <li><a class="idstatusklaim" 
                                      data-idstatusklaim="{{$sts->idstatus_klaim}}"
                                      data-idpengiriman = "{{$klaim->idpengiriman}}" 
                                      data-idbarang="{{$klaim->idbarang}}"
                                      href="#">{{$sts->status}}</a></li>
                                   <?php 
                                  } 
                                ?>
                              </ul>
                            </div>
                          <?php } ?>
                        
                          </td>
                         </tr>
                        <?php  
                      }
                    ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Kapal</th>
                  <th>Tanggal Trip</th>
                  <th>Container</th>
                  <th>Barang</th>
                  <th>Qty(Coly)</th>
                  <th>Pengirim</th>
                  <th>Penerima</th>
                  <th>Action</th>
                  <th>Status Klaim</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


      <!-- modal barang rusak -->
       <div class="modal fade" id="barangrusak" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <form role="form" method="POST" action="{{route('klaimpengiriman.uploadphotoandupdate')}}" enctype="multipart/form-data" onsubmit="return validate(this)">
                @csrf
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Barang Rusak</h4>
              </div>

                <div class="modal-body">
                  <input type="text" id="idpengrusak" name="idpengiriman" hidden>
                  <input type="text" id="idbrgrusak" name="idbarang" hidden>
                <div class="form-group">
                  <label for="exampleInputFile">File Photo</label>
                  <input type="file" id="fotobrgrusak" name="fotobrgrusak" required>
                </div>

                  <div class="form-group">
                  <label>Tanggal </label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="tglbarangrusak" name="tglbarangrusak" required>
                    </div>

                    <div class="box-header">
                      <h3 class="box-title">Keterangan Barang Rusak
                        <!-- <small>Bersifat Rahasia</small> -->
                      </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad">
                      <textarea id="keteranganbrgrusak" name="keteranganbrgrusak" required>
                                              
                      </textarea>
                    </div>
                  </div>
                </div>
                 <div class="modal-footer">
                      <button type="submit" class="btn btn-warning" >Update</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                 </div>
              </form>
            </div>
          </div>
        </div>


        <!-- modal view setelah update barang rusak -->
       <div class="modal fade" id="modalviewbarangrusak" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Barang Rusak</h4>
              </div>

                <div class="modal-body">
                  <input type="text" id="idpengbrgrusak" name="idbrgrusak" hidden>
                <div class="form-group">
                  <label>Photo</label><br>
                  <img id="imgviewbrgrusak" src="" style="width:100%; height:400px; object-fit: contain;">
                </div>

                <div class="box-body pad">
                    <textarea id="viewketeranganbrgrusak" name="viewketeranganbrgrusak" disabled>
                                            
                    </textarea>
                </div>

                <div class="form-group">
                <label>Tanggal </label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="viewtglbarangrusak" disabled>
                  </div>
                </div>

                </div>
                 <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                 </div>
            </div>
          </div>
        </div>

        <!-- modal barang hilang -->
        <div class="modal fade" id="baranghilang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <form role="form" method="POST" action="{{route('klaimpengiriman.uploadketerangantgl')}}" enctype="multipart/form-data" onsubmit="return validate(this)">
                @csrf
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Barang Hilang</h4>
              </div>
                <div class="modal-body">
                  <input type="text" id="idpenghilang" name="idpenghilang" hidden>
                  <input type="text" id="idbrghilang" name="idbrghilang" hidden>
                  <div class="box-header">
                    <h3 class="box-title">Keterangan Barang Hilang
                      <!-- <small>Bersifat Rahasia</small> -->
                    </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body pad">
                    <textarea id="keteranganbrghilang" name="keteranganbrghilang" required>
                                            
                    </textarea>
                  </div>
            
                 <div class="form-group">
                  <label>Tanggal </label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="tglbaranghilang" name="tglbaranghilang" required>
                  </div>
                 </div>

                </div>
                 <div class="modal-footer">
                      <button type="submit" class="btn btn-danger" >Update</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                 </div>
              </form>
            </div>
          </div>
        </div>


      <!-- modal view update barang hilang -->
        <div class="modal fade" id="modalviewbaranghilang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Barang Hilang</h4>
              </div>
                <div class="modal-body">
                  <input type="text" id="idpengbrghilang" name="idbrghilang" hidden>
                  <div class="box-header">
                    <h3 class="box-title">Keterangan Barang Hilang
                      <!-- <small>Bersifat Rahasia</small> -->
                    </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body pad">
                    <textarea id="viewketeranganbrghilang" readonly>
                                            
                    </textarea>
                  </div>
            
                 <div class="form-group">
                  <label>Tanggal </label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="viewtglbaranghilang" disabled>
                  </div>
                 </div>

                </div>
                 <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                 </div>
            </div>
          </div>
        </div>
      </section>

  </div>
@endsection
@section('script')
<!-- get id pengiriman barang rusak -->
<script type="text/javascript">
  $(".barangrusak").on("click", function(e){
    var idpeng = $(this).data("idpengiriman");
    var idbrg = $(this).data("idbarang");
    $("#idpengrusak").val(idpeng);
    $("#idbrgrusak").val(idbrg);
    
  });
</script>
<!-- get id pengiriman barang hilang -->
<script type="text/javascript">
  $(".baranghilang").on("click", function(e){
    var idpeng = $(this).data("idpengiriman");
    var idbrg = $(this).data("idbarang");
    $("#idpenghilang").val(idpeng); 
    $("#idbrghilang").val(idbrg);
  });
</script>

<!-- btn view untuk barang rusak  -->
<script type="text/javascript">
  $(".btnviewbrgrusak").on("click", function(e){
    // get
    var fotobrgrusak = $(this).data('fotobrgrusak');
    var tglupload = $(this).data('tglbrgrusak');
    var keteranganbrgrusak = $(this).data('keteranganbrgrusak');

    // set
    document.getElementById('imgviewbrgrusak').src='data_file/'+fotobrgrusak;
    CKEDITOR.instances['viewketeranganbrgrusak'].setData(keteranganbrgrusak);
    $("#viewtglbarangrusak").val(tglupload);

    // show
    $("#modalviewbarangrusak").modal("show");    
  });
</script>
<script type="text/javascript">
   $('#modalviewbaranghilang').bind('show',function(){
      $("#nome").val('bosta');
  });
</script>
<!-- btn view untuk barang hilang  -->
<script type="text/javascript">
  $(".btnviewbrghilang").on("click", function(e){ 
    $("viewketeranganbrghilang").val("");
    // getx`
    var ketbrghilang = $(this).data('ketbrghilang');
    var tglhilang = $(this).data('tglbrghilang');

    // show
    $("#modalviewbaranghilang").modal("show");    
    $('#modalviewbaranghilang').on('shown.bs.modal', function() {
      // $(this).find('#viewketeranganbrghilang').val(ketbrghilang);
      CKEDITOR.instances['viewketeranganbrghilang'].setData(ketbrghilang);
      $("#viewtglbaranghilang").val(tglhilang);
      
    });
    
  });
</script>


<script type="text/javascript">
  function validate(form) {
    if (confirm("Update Data?")) {
      return true;
      
    }
    else
    {
      return false;
    }
  }
</script>

<script type="text/javascript">
  var date = new Date();
  date.setDate(date.getDate()-1);
   $('#tglbaranghilang').datepicker({
      startDate : date,
      autoclose: true,
      format: 'dd-mm-yyyy'
      
    })
      $('#tglbarangrusak').datepicker({
      startDate : date,
      autoclose: true,
      format: 'dd-mm-yyyy'
      
    })

</script>
<script type="text/javascript">
  $(".idstatusklaim").on("click", function(e){
    var idstatus     = $(this).data("idstatusklaim");
    var idpengiriman = $(this).data("idpengiriman");
    var idbarang     = $(this).data("idbarang");
    // alert(idstatus);
    var url = "{{route('klaim.updatestatusklaim')}}"
    if (confirm("Ingin Update Status Klaim?")) 
    {
       $.ajaxSetup({
                  headers: {
                          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                      }
                  });
       $.ajax({
        url: url,
        type:'POST',
        data:{idstatus, idpengiriman, idbarang},
        }).done(function(response){
          alert("Data berhasil di Update");
          location.reload();
        });
      }
      else
      {
        return false;
      }
  });
</script>

<script type="text/javascript">
  $('#klaimdatatable thead th').each( function () {
        var title = $('#klaimdatatable tfoot th').eq( $(this).index() ).text();
        if(title!=""){
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
      }
    } );
 
    // DataTable
    var table = $('#klaimdatatable').DataTable({
    columnDefs: [
        { searchable: false, targets: 7,
          width : 250, targets : 7},
        { searchable: false, targets: 8,
        width : 250, targets : 8},
        // date format
        {  render: function(data) {
            return moment(data).format('DD-MM-YYYY');
          }, targets: 1
        }
    ],
    scrollX:        true,
    scrollCollapse: true,
    fixedColumns: true
    });
 
    // Apply the search
    table.columns().eq( 0 ).each( function ( colIdx ) {
        if( !table.settings()[0].aoColumns[colIdx].bSearchable ){
        table.column( colIdx ).header().innerHTML=table.column( colIdx ).footer().innerHTML;
    }
        $( 'input', table.column( colIdx ).header() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    } );
</script>

@endsection

