@extends('layouts.layout')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Home
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#">welcome</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
   

        <!-- Jabatan -->
        <div class="box-body">
          <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Pengiriman</h3>

              <p>Menu Pengiriman</p>
            </div>
            <div class="icon">
              <i class="fa fa-send"></i>
            </div>
            <a href="{{route('pengiriman.index')}}" class="small-box-footer">Click Here <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
               <h3>Packing List</h3>

              <p>Menu Packing List</p>
            </div>
            <div class="icon">
              <i class="fa fa-cube"></i>
            </div>
            <a href="{{route('packinglist.index')}}" class="small-box-footer">Click Here <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

          <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Status Pengiriman</h3>

              <p>Menu Status Pengiriman</p>
            </div>
            <div class="icon">
              <i class="fa fa-check"></i>
            </div>
            <a href="{{route('statuspengiriman.index')}}" class="small-box-footer">Click Here <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
       <!--  <div class="col-lg-6 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Document</h3>

              <p>Menu Document</p>
            </div>
            <div class="icon">
              <i class="fa fa-sticky-note-o"></i>
            </div>
            <a href="{{route('document.index')}}" class="small-box-footer">Click Here <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div> -->

          <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Klaim</h3>

              <p>Menu Klaim Barang</p>
            </div>
            <div class="icon">
              <i class="fa  fa-exclamation"></i>
            </div>
            <a href="{{route('klaim.index')}}" class="small-box-footer">Click Here <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>


        </div>



        <!-- /.box-body -->
       <!--  <div class="box-footer">
         <button type="button" class="btn btn-success">Submit</button>
        </div> -->

        <!-- /.box-footer-->
      <!-- tempat div -->
      <!-- /.box -->

    </section>

 </div>

@endsection
