@extends('layouts.layout')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Packing List
        <!-- <small>Control panel</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Packing List</li>
      </ol>
    </section>
      <section class ="content">
       <div class="row">
        <div class="col-xs-12">

        @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong>{{ $message }}</strong>
          </div>
        @endif

        @if ($message = Session::get('error'))
          <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
        @endif

        @if ($message = Session::get('warning'))
          <div class="alert alert-warning alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
        @endif

        @if ($message = Session::get('info'))
          <div class="alert alert-info alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
        @endif

        @if ($errors->any())
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            Please check the form below for errors
        </div>
        @endif

          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Packing List Kapal dan Container</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tablemultiplepacking" class="table table-bordered table-striped tablepackinglist ">
                <thead>
                <tr>
                  <!-- <th><input type="checkbox"  id="selectall" name="checkboxselectall"></th> -->
                  <th>Kapal</th>
                  <th>Trip</th>
                  <th>Kota Asal</th>
                  <th>Kota Tujuan</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    for ($i=0;$i<count($pengiriman); $i++)
                    {
                     $pengirimans=$pengiriman[$i];
                      ?>
                        <tr>
                          <!-- <td><input type="checkbox" id="checkboxrow" name="checkboxpackinglist"></td> -->
                            <td>
                              <?php
                                echo $pengirimans->kapal;
                              ?>
                            </td>
                            <td>
                              <?php
                                echo $pengirimans->tgl_pengiriman;
                              ?>
                            </td>
                            <td>
                              <?php
                                echo $pengirimans->kota_pengirim;
                              ?>
                            </td>
                            <td>
                              <?php
                                echo $pengirimans->kota_penerima;
                              ?>
                            </td>
                            <td>
                            <button  class="btn bg-purple  opendetailpengiriman"  
                              data-toggle="modal" data-target="#detailpengiriman"
                              data-idpengiriman="{{$pengirimans->idpengiriman}}"
                              data-namakapal = "{{$pengirimans->kapal}}"
                              data-tglpengiriman = "{{$pengirimans->tgl_pengiriman}}"
                              data-statuspengiriman = "{{$pengirimans->status_pengiriman_idstatus_pengiriman}}"
                              data-statusprint = "{{$pengirimans->status_print}}"
                              data-kotapengirim = "{{$pengirimans->kota_pengirim}}"
                              data-kotapenerima = "{{$pengirimans->kota_penerima}}"
                              data-keteranganoffice = "{{$pengirimans->keterangan_office}}"
                              data-keterangandriver = "{{$pengirimans->keterangan_driver}}"
                              >Detail Packing List </button>
                            </td>
                            
                        </tr>
                      <?php
                    }
                  ?>
                </tbody>
                <tfoot>
                 <tr>
                  <!-- <th></th> -->
                  <th>Kapal</th>
                  <th>Trip</th>
                  <th>Kota Asal</th>
                  <th>Kota Tujuan</th>
                  <th>Action</th>
                 </tr>
                </tfoot>
              </table>
            </div>
            
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>




<!--------------------------------------------------------------------------------------------------------------------- MODAL ----------------------------------------------------------------------------------------------------->
<!-- MODAL DETAIL PENGIRIMAN -->
      <div class="modal fade" id="detailpengiriman" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Container</h4>
              </div>
                <form role="form" method ="POST" id="prosespdf" action="{{route('packinglist.cetak_pdf')}}" onsubmit="return validate(this)">
                  @csrf                  
                  <div class="modal-body">
                    <div class="form-group">
                       <table id="tablenormal" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>Nama Container</th>
                          <th>Kode Seal</th>
                          <th>Nama Pengirim</th>
                          <th>Nama Penerima</th>
                          <th>Barang</th>
                          <th>Qty Coly</th>
                          <th>Kubikasi</th>
                          <th>Tonase/Meter</th>
                          <th>Jenis Pengiriman</th>
                          <!-- <th>Tonase/Meter</th> -->

                        </tr>
                        </thead>
                      </table>
                      <!-- input seal kapal juga pakai ini -->
                      <input type="text" name="namakapaluntukprint"id="namakapaluntukprint" hidden>
                      <input type="text" name="tglkirimprint"id="tglkirimprint" hidden>
                      <input type="text" name="kotapengirimprint"id="kotapengirimprint" hidden>
                      <input type="text" name="kotapenerimaprint"id="kotapenerimaprint" hidden>

                      <!-- button seal untuk auto fill seal -->
                    <button  type ="button" id="sealfill" name="sealfill" class="btn bg-purple col-sm-2 openfillseal"  data-toggle="modal" data-target="#fillsealmodal" style="float:left; margin-left: 2px;">Input Seal</button>
                    </div> <br><br>
                  </div>
                  <input type="text" id="valuejeniscontainer20ft" name="valuejeniscontainer20ft" hidden>
                  <input type="text" id="valuejeniscontainer40ft" name="valuejeniscontainer40ft" hidden>
                  <input type="text" id="valuejeniscontainer40HQ" name="valuejeniscontainer40HQ" hidden>
                  <input type="text" id="valuejeniscontainer21ft" name="valuejeniscontainer21ft" hidden>
                  <input type="text" id="valuejeniscontainerRefer" name="valuejeniscontainerRefer" hidden>


                  <!-- <input type="text" id="namakapalapproval" name="namakapalapproval" hidden> -->

                  <input type="text" id="idpengirimanutkpdf" name="idpengirimanutkpdf" hidden>

                  <!-- catatan untuk Driver -->
                   <div class="box-header">
                    <h3 class="box-title">Keterangan Untuk Driver
                      <!-- <small>Bersifat Umum</small> -->
                    </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body pad">
                    <textarea id="keterangandriver" name="keterangandriver" rows="10" cols="80" required>
                                            
                    </textarea>
                  </div>

                  <!-- catatan untuk Office -->
                  <div class="box-header">
                    <h3 class="box-title" >Keterangan Untuk Office
                      <!-- <small>Bersifat Rahasia</small> -->
                    </h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body pad">
                    <textarea id="keteranganoffice" name="keteranganoffice" rows="10" cols="80" required>
                                            
                    </textarea>
                  </div>
                 <div class="modal-footer">
                  <div id="divprosespengiriman">
                      <button  type ="submit" id="prosespengiriman" name="prosespengiriman" class="btn bg-purple prosespengiriman" >Print PDF Pengiriman</button>
                      <button type="button" class="btn btn-default closetablekapal" data-dismiss="modal">Close</button> 
                  </div>
                  <div id="divsucccesspengiriman">
                      <button type="button" id="succcesspengiriman" class="btn btn-success succcesspengiriman" disabled>Pengiriman Barang Sudah Diproses</button>
                      <button type="button" class="btn btn-default closetablekapal" data-dismiss="modal">Close</button> 
                  </div>
                  <div id="divpengirimanapproved">
                      <button type="button" id="approvedpengiriman" class="btn btn-danger approvedpengiriman">Print PDF</button>
                      <button type="button" class="btn btn-default closetablekapal" data-dismiss="modal">Close</button> 
                  </div>
                 </div>
               </form>
            </div>
          </div>
        </div> 
<!-- /MODAL DETAIL PENGIRIMAN -->
<!-- MODAL INPUT SEAL -->
         <div class="modal fade" id="fillsealmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
           <div class="modal-dialog" role="document">
             <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Seal</h4>
              </div>
                <form id="formfillseal" role="form" method ="POST" action="{{route('packing.submitfillseal')}}" onsubmit="return validateseal(this)">
                  @csrf                  
                  <div class="modal-body">
                    <table id="tablefillseal" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>Nama Container</th>
                                <th>Kode Seal</th>
                              </tr>
                            </thead>
                      </table>
                  </div>
                  <div class="modal-footer">
                    <div id="divpengirimanapproved">
                      <button type="submit" id="submitfillseal" class="btn bg-purple">Submit</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
<!-- /MODAL INPUT SEAL -->

<!-- MODAL APPROVAL -->
        <div class="modal fade" id="approval" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Approval</h4>
              </div>               
                <div class="modal-body">    
                  <label>Sisa Container <i>(Coly)</i></label> <br>
                  <small>20 ft</small><br>
                  <input type="text" id="input20ft" class="form-control" disabled> 
                  <small>40 ft</small><br>
                  <input type="text" id="input40ft" class="form-control" disabled> 
                  <small>40 High Cube</small><br>
                  <input type="text" id="input40hq" class="form-control" disabled> 
                  <small>21 ft</small><br>
                  <input type="text" id="input21ft" class="form-control" disabled> 
                  <small>Refer</small><br>
                  <input type="text" id="inputrefer" class="form-control" disabled> 
                  <br>  
                  <label>List User Approval <small><br><i>(Approval hanya untuk user yang memiliki jabatan sebagai Direksi atau Supervisor)</i></small></label> <br>
                    <div class="form-group has-feedback">   
                      <select id="idpengirimanapproval" class="form-control" data-placeholder="NameJab" name="NameJab" style="width: 100%;" required>
                        <?php 
                          for($i=0; $i<count($user); $i++)
                          {
                            $users = $user[$i];
                            if ($users->jabatan_idjabatan==2 || $users->jabatan_idjabatan==3 ) {
                            ?>
                            <option value="{{$users->id}}">
                              <?php echo $users->name ?>
                            </option>
                           <?php 
                          } 
                        }
                        ?>

                      </select>
                    </div>
                    <div class="form-group has-feedback">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder ="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                      @error('password')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                    </div>
                    <form id="formcheckidpengiriman" hidden >
                      <input type="text" id="iduser" name="iduser" hidden>
                      <input type="text" id="pass" name="pass" hidden>
                      <input type="text" id="namakapalapproval" name="namakapalapproval" hidden>
                      <table id="tableinputseal" class="table table-bordered table-striped" hidden="hidden">
                            <thead>
                              <tr>
                                <th>Nama Container</th>
                                <th>Kode Seal</th>
                                <th>Nama Pengirim</th>
                                <th>Nama Penerima</th>
                                <th>Barang</th>
                                <th>Jenis Pengiriman</th>
                              </tr>
                            </thead>
                      </table>
                      <!-- /.box-header -->
                      <div class="box-body pad">
                        <textarea id="keterangandriverapproval" name="keterangandriverapproval" placeholder="Keterangan Untuk Driver" rows="10" cols="80" required>
                                                
                        </textarea>
                      </div>
                      <div class="box-body pad">
                        <textarea id="keteranganofficeapproval" name="keteranganofficeapproval" placeholder="Keterangan Untuk Office" rows="10" cols="80" required>
                                                
                        </textarea>
                      </div>
                    <!-- checkidpengiriman -->
                      <div id="checkidpengiriman">

                      </div>
                    </form>
                  </div>

                 <div class="modal-footer">
                      <button type="button" id="checkapproval" class="btn bg-purple">Approve</button>
                      <button type="button" class="btn btn-default closetablekapal" data-dismiss="modal">Close</button> 
                 </div>
               <!-- </form> -->
            </div>
          </div>
        </div> 
      </section>

  </div>
@endsection
@section('script')
<!-- select all checkbox -->
<script type="text/javascript">
 $('#tablemultiplepacking thead th').each( function () {
    var title = $('#tablemultiplepacking tfoot th').eq( $(this).index() ).text();
    if(title!=""){
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
      }

    });
    //  DataTable tablenormal
    $("#tablenormal").DataTable({
      scrollX:true
    });

    // DataTable tablemultiplepacking
    var table = $('#tablemultiplepacking').DataTable({
    columnDefs: [
        { 
          searchable: false, targets: 4
        },
        {
          width: "100%", targets : 0,
        },
        // date format
        {  render: function(data) {
            return moment(data).format('DD-MM-YYYY');
          }, targets: 1
        }
    ],
    select: {
         'style': 'multi'
      },
    order: [1, 'asc'],

    drawCallback: function(settings)
      {
        // //iCheck for checkbox tapi gak jadi
        // $('input[type="checkbox"]').iCheck({
        //    checkboxClass: 'icheckbox_minimal-purple'
        // });
        // //iCheck for checked box all row
        // $('#selectall').on('ifChanged', function(event){
        //   if( $(this).is(':checked')){
        //     $('.tablepackinglist tbody input[type="checkbox"]').iCheck('check');
        //   }
        //   else 
        //   {
        //     $('.tablepackinglist tbody input[type="checkbox"]').iCheck('uncheck');
        //   }
        // });

          $('.opendetailpengiriman').on('click',function(e){
            // set value 0 to check approval
          $("#valuejeniscontainer20ft").val("");
          $("#valuejeniscontainer40ft").val("");
          $("#valuejeniscontainer40HQ").val("");
          $("#valuejeniscontainer21ft").val("");
          $("#valuejeniscontainerRefer").val("");
          $('#keteranganoffice').val(" ");
          $('#keterangandriver').val(" ");

          var idpengiriman = $(this).data("idpengiriman");
          $("#idpengirimanutkpdf").val(idpengiriman);

          var namakapal         = $(this).data("namakapal");
          var tglkirim          = $(this).data("tglpengiriman");
          var kotapengirim      = $(this).data("kotapengirim");
          var kotapenerima      = $(this).data("kotapenerima");

          // status pengiriman check disabled button print
          var statuspengiriman = $(this).data("statuspengiriman");
          var statusprint      = $(this).data("statusprint");
          var keteranganoffice = $(this).data("keteranganoffice");
          var keterangandriver = $(this).data("keterangandriver");


          // if else status pengiriman 0=belum terkirim
          if(statuspengiriman==0 && statusprint==0)
          {
            document.getElementById("divpengirimanapproved").style.display='none';
            document.getElementById("divsucccesspengiriman").style.display='none';
            document.getElementById("divprosespengiriman").style.display='block';
          }
           else if(statuspengiriman==1 && statusprint==1)
          {
            document.getElementById("divpengirimanapproved").style.display='none';
            document.getElementById("divsucccesspengiriman").style.display='block';
            document.getElementById("divprosespengiriman").style.display='none';
            $('.succcesspengiriman').attr('disabled','disabled');            
          }
          else 
          {
            document.getElementById("divpengirimanapproved").style.display='block';
            document.getElementById("divsucccesspengiriman").style.display='none';
            document.getElementById("divprosespengiriman").style.display='none';
          }
         

          $("#namakapaluntukprint").val(namakapal); 
          $("#tglkirimprint").val(tglkirim); 
          $("#kotapengirimprint").val(kotapengirim);
          $("#kotapenerimaprint").val(kotapenerima);
          $("#namakapalapproval").val(namakapal);

          // set value ckeditor... gak bisa on click apabila cuman .val()
          CKEDITOR.instances['keteranganoffice'].setData(keteranganoffice);
          CKEDITOR.instances['keterangandriver'].setData(keterangandriver);
          
          

          var url = '{{route("packing.showdetailpengirimankapal")}}';
          $.ajax({
              url: url,
              type:'POST',
              data:{namakapal,tglkirim,kotapengirim,kotapenerima},

            }).done(function(response){
              var id=0;
              $('#tablenormal').dataTable().fnClearTable();
              $.each(response, function (index, value) {
                id++;
                   // pagination tidak berfungsi
                   // tr += '<tr><td>' + value.nama_container + '</td><td>' +  value.nama_pengirim + '</td><td>' +  value.nama_penerima + '</td><td>'+value.nama_barang+ '</td><td>' +value.nama_jenis_pengiriman+'</td></tr>';
                   // alert(value.seal);
                   // IF ELSE CHECK SEAL NULL APA GAK
                   if (value.seal!=null) {
                    // FILE TYPE INPUT SEAL DIISI VALUE.SEAL AGAR PDF JALAN (INPUT JADI TIDAK KOSONG)
                    $("#tablenormal").DataTable().row.add([
                        value.nama_container, value.seal+
                        `<input type="text" size="10" id="kodeseal-`+id+`" name="kodeseal[`+id+`][seal]" class="kodeseal" value="`+value.seal+`" hidden>
                         <input type="text" size="10" id="idbarang-`+id+`" name="kodeseal[`+id+`][idbarang]" value="`+value.idbarang+`" hidden>`,value.nama_pengirim, value.nama_penerima, value.nama_barang, value.qty_barang,value.total_kubikasi, value.keterangan_tonase_meter, value.nama_jenis_pengiriman
                    ]).draw();

                    // ttp dimasukan id pengiriman untuk input keterangan driver dkk
                    $("#checkidpengiriman").append(`<input type="text" size="10" id="idpengirimanapproval" name="idpengirimanapproval[`+id+`][idpengirimanapproval]" class="idpengirimanapproval" value="`+value.idpengiriman+`" hidden>`);

                    // set style buat buttol fill seal
                    document.getElementById("sealfill").style.display='none';
                   }
                   // APABILA SEAL NULL
                   else 
                   {
                    $("#tablenormal").DataTable().row.add([
                        value.nama_container,
                        `<input type="text" size="10" id="kodeseal-`+id+`" name="kodeseal[`+id+`][seal]" class = "kodeseal"required>
                         <input type="text" size="10" id="idbarang-`+id+`" name="kodeseal[`+id+`][idbarang]" value="`+value.idbarang+`"hidden>` ,value.nama_pengirim, value.nama_penerima, value.nama_barang, value.qty_barang,value.total_kubikasi,value.keterangan_tonase_meter, value.nama_jenis_pengiriman
                    ]).draw();


                    $("#checkidpengiriman").append(`<input type="text" size="10" id="idpengiriman" name="idpengiriman[`+id+`][idpengiriman]" class="idpengirimanapproval" value="`+value.idpengiriman+`">`);

                    // HIDDEN UNTUK SET SEAL KEADALAM DB BIAR GAK 2 KALI SETEALH APPROVAL
                    $("#tableinputseal").DataTable().row.add([
                        value.nama_container,
                        `<input type="text" size="10" id="kodesealapproval-`+id+`" name="sealapproval[`+id+`][sealapproval]" required>
                         <input type="text" size="10" id="idbarangapproval-`+id+`" name="sealapproval[`+id+`][idbarangapproval]" value="`+value.idbarang+`"hidden>` ,value.nama_pengirim, value.nama_penerima, value.nama_barang, value.nama_jenis_pengiriman
                    ]).draw();

                    $("#checkidpengiriman").append(`<input type="text" size="10" id="idpengirimanapproval" name="idpengirimanapproval[`+id+`][idpengirimanapproval]" class="idpengirimanapproval" value="`+value.idpengiriman+`" hidden>`);

                    // set style buat buttol fill seal
                    document.getElementById("sealfill").style.display='block';
                   }

                   //set approval check untuk tiap2 jenis container
                   if(value.jenis_container_idcontainer==7){
                      $("#valuejeniscontainer20ft").val(value.sisa_container_temp);
                   }
                   else if(value.jenis_container_idcontainer==8){
                      $("#valuejeniscontainer40ft").val(value.sisa_container_temp);
                   }
                   else if(value.jenis_container_idcontainer==9){
                      $("#valuejeniscontainer40HQ").val(value.sisa_container_temp);
                   }
                   else if (value.jenis_container_idcontainer==10){
                      $("#valuejeniscontainer21ft").val(value.sisa_container_temp);
                   }
                   else{
                      $("#valuejeniscontainerRefer").val(value.sisa_container_temp);
                   }

                   // $("#valuejeniscontainer").val(value.update);
                 });
                 // pagination tidak berfungsi
                 // $('#appenddatakapal').html(tr);
            });

         });
     }
    });

    // Restore state after page refresh
    var state = table.state.loaded();
    if (state) {
        table.columns().eq(0).each(function (colIdx) {
            var colSearch = state.columns[colIdx].search;

            if (colSearch.search) {
                $('input', table.column(colIdx).header()).val(colSearch.search);
            }
        });

        table.draw();
    }

    // Apply the search
    table.columns().eq( 0 ).each( function ( colIdx ) {
        if( !table.settings()[0].aoColumns[colIdx].bSearchable ){
        table.column( colIdx ).header().innerHTML=table.column( colIdx ).footer().innerHTML;
    }
        $( 'input', table.column( colIdx ).header() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    });
    $('#tablemultiplesearch_filter').hide();

</script>
<!-- modal seal fill -->
<script type="text/javascript">
  $('.openfillseal').on('click',function(e){

    var namakapalfillseal    = $("#namakapaluntukprint").val();
    var tglkirimfillseal     = $("#tglkirimprint").val();
    var kotapengirimfillseal = $("#kotapengirimprint").val();
    var kotapenerimafillseal = $("#kotapenerimaprint").val();

    var formdataid = new FormData($('#formfillseal')[0]);
    var url = "{{route('packing.fillsealshow')}}";
    $.ajaxSetup({
              headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
    $.ajax({
      url:url,
      type : "POST",
      data :{namakapalfillseal,tglkirimfillseal,kotapengirimfillseal,kotapenerimafillseal}
    }).done(function(response){
          var id=0;
          $('#tablefillseal').dataTable().fnClearTable();
          $.each(response, function (index, value) 
          {
            id++;
            $("#tablefillseal").DataTable().row.add([
                    value.nama_container, `<input type="text" size="10" id="inputfillseal-`+id+`" name="fillseal[`+id+`][inputfillseal]" required>
                    <input type="text" size="10" id="idnamakapal-`+id+`" name="fillseal[`+id+`][namakapal]" value="`+value.kapal+`" hidden>
                    <input type="text" size="10" id="idcontainer-`+id+`" name="fillseal[`+id+`][container]" value="`+value.nama_container+`" hidden>
                    `]).draw();

          });
    });

  });
</script>
<script>
  $(function () {
    $('#tablenormal').DataTable()
  })
</script>

<!-- set per id setiap value kode sel input awal dan table kode seal didalam approval -->
<script type="text/javascript">
  $(document).on('input','.kodeseal',function(){
  var a = document.getElementsByClassName("kodeseal");
  var id =0;
  for(var i=0; i<a.length; i++)
  {
    id++;
    var valkodeseal = $("#kodeseal-"+id).val();
    $("#kodesealapproval-"+id).val(valkodeseal);

  }
  });
</script>

<!-- proses pengiriman -->
<script type="text/javascript">
function validate(form) {

      var jk20ft                   = $("#valuejeniscontainer20ft").val();
      var jk40ft                   = $("#valuejeniscontainer40ft").val();
      var jk40HQ                   = $("#valuejeniscontainer40HQ").val();
      var jk21ft                   = $("#valuejeniscontainer21ft").val();
      var jkRef                    = $("#valuejeniscontainerRefer").val();
      var keteranganofficeapproval = CKEDITOR.instances["keteranganoffice"].getData();
      var keterangandriverapproval = CKEDITOR.instances["keterangandriver"].getData();

      $("#input20ft").val(jk20ft);
      $("#input40ft").val(jk40ft);
      $("#input40hq").val(jk40HQ);
      $("#input21ft").val(jk21ft);
      $("#inputrefer").val(jkRef);

      $("#keteranganofficeapproval").val(keteranganofficeapproval);
      $("#keterangandriverapproval").val(keterangandriverapproval);
      

      if (confirm("Cetak Proses Pengiriman ?")) {
        

        if (jk20ft>0||jk40ft>0||jk40HQ>0||jk21ft>0||jkRef>0) {
          $("#approval").modal("show");
          return false;
          // check password di ajax done function
        }
        else
        {
          return true;
        }
      }
      else
      {
        return false;
      }
}
</script>

<script type="text/javascript">
function validateseal(form) {
      if (confirm("Masukan Data Seal ?")) {
        return true;
      }
      else
      {
        return false;
      }
}
</script>

<!-- check approfal and insert seal -->
<script type="text/javascript"> 
    $("#checkapproval").on("click", function(e){
    var url = "{{route('packing.approval')}}";
    var iduser = $("#idpengirimanapproval").val();
    var pass = $("#password").val();
    $("#iduser").val(iduser);
    $("#pass").val(pass);

    var formdataid = new FormData($('#formcheckidpengiriman')[0]);
    $.ajaxSetup({
              headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
    $.ajax({
      url:url,
      type : "POST",
      data:formdataid,
      async: false,
      cache: false,
      contentType: false,
      enctype: 'multipart/form-data',
      processData: false,
    }).done(function(e){
        if(e==1)
        {
          alert("Password Diterima");
          location.reload();
        }
        else
        {
          alert("Password Salah");
          return false;
        }
    });
  });
</script>
<!-- APPROVED PRINT -->
<script type="text/javascript"> 
    $("#approvedpengiriman").on("click", function(e){
      var form = $("#prosespdf");
      $("#prosespdf").removeAttr("onsubmit"); //remove form onsubmit(pengecheckan)
      if(confirm("Print PDF Pengiriman?"))
      {
        form.submit();
      }
      else
      {
        return false;
      }
      
    });
</script>

@endsection

