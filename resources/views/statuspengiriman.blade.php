@extends('layouts.layout')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Status Pengiriman
        <!-- <small>Control panel</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Status Pengiriman</li>
      </ol>
     </section>
      <section class ="content">

      <div class="row">
        <div class="col-xs-12">
        @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong>{{ $message }}</strong>
          </div>
        @endif

        @if ($message = Session::get('error'))
          <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
        @endif

        @if ($message = Session::get('warning'))
          <div class="alert alert-warning alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
        @endif

        @if ($message = Session::get('info'))
          <div class="alert alert-info alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
        @endif

        @if ($errors->any())
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            Please check the form below for errors
        </div>
        @endif
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Status Pengiriman</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="multiplesearchstatuspengiriman" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Tanggal Trip</th>
                  <th>Kapal</th>
                  <th>Container</th>
                  <th>Pengirim</th>
                  <th>Penerima</th>
                  <th>Barang</th>
                  <th>Status Barang</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                      for ($i=0; $i<count($statuspengiriman); $i++) 
                      { 
                          $stspeng=$statuspengiriman[$i];
                        ?>   
                        <tr>
                          <td>
                             <?php 
                                 echo $stspeng->tgl_pengiriman;
                              ?>
                          </td>
                          <td>
                             <?php 
                                 echo $stspeng->kapal;
                              ?>
                          </td>
                          <td>
                             <?php 
                                 echo $stspeng->nama_container;
                              ?>
                          </td>
                          <td>
                             <?php 
                                 
                                 echo $stspeng->nama_pengirim;
                              ?>
                          </td>
                          <td>
                             <?php 
                                 echo $stspeng->nama_penerima;
                              ?>
                          </td>
                          <td>
                             <?php 
                                 echo $stspeng->nama_barang;
                              ?>
                          </td>
                          <td >
                            <!-- status for different colour -->
                            <!-- Status Kirim -->
                            <?php if ($stspeng->status_pengiriman_idstatus_pengiriman== 1)
                            { 
                              ?>
                             <div class="btn-group">
                              <button type="button" class="btn btn-info "><?php echo $stspeng->status ?></button>
                              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <?php 
                                 // $first= true;
                                 foreach($dropdownstatus as $ds)
                                  {
                                    // hide first element
                                    // if ($first) { $first=false; continue; }
                                    // data-kapal(untuk keperluan update sesuai nama kapal)
                                   ?>
                                    <li><a class="idstatusget"
                                      data-idpengiriman = "{{$stspeng->idpengiriman}}" 
                                      data-idbarang = "{{$stspeng->barang_idbarang}}" 
                                      data-qtybarangkirim = "{{$stspeng->qty_barang}}"
                                      data-idstatus="{{$ds->idstatus_pengiriman}}"
                                      data-idstatus1="{{$dropdownstatus[2]->idstatus_pengiriman}}"
                                      data-kapal ="{{$stspeng->kapal}}"
                                      href="#">{{$dropdownstatus[2]->status}}</a></li>
                                   <?php 
                                   break;
                                  } 
                                ?>
                              </ul>
                            </div>
                            <?php
                             } 
                             // Status Tiba
                             else if ($stspeng->status_pengiriman_idstatus_pengiriman== 2)
                             {
                             ?>
                             <div class="btn-group">
                                <button type="button" class="btn bg-orange"><?php echo $stspeng->status ?></button>
                                <button type="button" class="btn bg-orange dropdown-toggle" data-toggle="dropdown">
                                  <span class="caret"></span>
                                  <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                <?php 
                                // $first= true;
                                 foreach($dropdownstatus as $ds)
                                  {
                                    // hide first element
                                    // if ($first) { $first=false; continue; }
                                   ?>
                                    <li><a class="idstatusget"
                                      data-idpengiriman = "{{$stspeng->idpengiriman}}"
                                      data-idbarang = "{{$stspeng->barang_idbarang}}" 
                                      data-qtybarangkirim = "{{$stspeng->qty_barang}}"
                                      data-idstatus="{{$ds->idstatus_pengiriman}}"
                                      data-idstatus1="{{$dropdownstatus[3]->idstatus_pengiriman}}"
                                      data-container="{{$stspeng->nama_container}}"
                                      data-kapal ="{{$stspeng->kapal}}"
                                      href="#">{{$dropdownstatus[3]->status}}</a></li>
                                   <?php 
                                   break;
                                  } 
                                ?>
                                </ul>
                              </div>
                             <?php
                             } 
                             // Status Bongkar di Office Cab
                             else if ($stspeng->status_pengiriman_idstatus_pengiriman== 3)
                             {
                             ?>
                             <div class="btn-group">
                                <button type="button" class="btn bg-navy"><?php echo $stspeng->status ?></button>
                                <button type="button" class="btn bg-navy dropdown-toggle" data-toggle="dropdown">
                                  <span class="caret"></span>
                                  <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                <?php 
                                // $first= true;
                                 foreach($dropdownstatus as $ds)
                                  {
                                    // hide first element
                                    // if ($first) { $first=false; continue; }
                                   ?>
                                      <!-- barang Digudang -->
                                      <li><a class="idstatusget"
                                      data-idpengiriman = "{{$stspeng->idpengiriman}}"
                                      data-idbarang = "{{$stspeng->barang_idbarang}}" 
                                      data-qtybarangkirim = "{{$stspeng->qty_barang}}" 
                                      data-idstatus="{{$ds->idstatus_pengiriman}}"
                                      data-idstatus1="{{$dropdownstatus[4]->idstatus_pengiriman}}"
                                      href="#">{{$dropdownstatus[4]->status}}</a></li>

                                      <!-- barang Dikirim ke Relasi -->
                                      <li><a class="idstatusget"
                                      data-idpengiriman = "{{$stspeng->idpengiriman}}"
                                      data-idbarang = "{{$stspeng->barang_idbarang}}" 
                                      data-qtybarangkirim = "{{$stspeng->qty_barang}}" 
                                      data-idstatus="{{$ds->idstatus_pengiriman}}"
                                      data-idstatus1="{{$dropdownstatus[5]->idstatus_pengiriman}}"
                                      href="#">{{$dropdownstatus[5]->status}}</a></li>
                                   <?php 
                                   break;
                                  } 
                                ?>
                                </ul>
                              </div>
                             <?php
                             } 
                             // Status Barang Digudang
                             else if ($stspeng->status_pengiriman_idstatus_pengiriman== 4)
                             {
                             ?>
                             <div class="btn-group">
                                <button type="button" class="btn btn-danger"><?php echo $stspeng->status ?></button>
                                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                  <span class="caret"></span>
                                  <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                <?php 
                                // $first= true;
                                 foreach($dropdownstatus as $ds)
                                  {
                                    // hide first element
                                    // if ($first) { $first=false; continue; }
                                   ?>
                                    <li><a class="idstatusget"
                                      data-idpengiriman = "{{$stspeng->idpengiriman}}"
                                      data-idbarang = "{{$stspeng->barang_idbarang}}" 
                                      data-qtybarangkirim = "{{$stspeng->qty_barang}}" 
                                      data-idstatus="{{$ds->idstatus_pengiriman}}"
                                      data-idstatus1="{{$dropdownstatus[5]->idstatus_pengiriman}}"
                                      href="#">{{$dropdownstatus[5]->status}}</a></li>
                                   <?php 
                                   break;
                                  } 
                                ?>
                                </ul>
                              </div>
                             <?php
                             } 
                             // Status Kirim Ke Relasi
                             else if ($stspeng->status_pengiriman_idstatus_pengiriman== 5)
                             {
                             ?>
                             <div class="btn-group">
                                <button type="button" class="btn bg-maroon"><?php echo $stspeng->status ?></button>
                                <button type="button" class="btn bg-maroon dropdown-toggle" data-toggle="dropdown">
                                  <span class="caret"></span>
                                  <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                <?php 
                                // $first= true;
                                 foreach($dropdownstatus as $ds)
                                  {
                                    // hide first element
                                    // if ($first) { $first=false; continue; }
                                   ?>
                                    <li><a class="idstatusget"
                                      data-idpengiriman = "{{$stspeng->idpengiriman}}"
                                      data-idbarang = "{{$stspeng->barang_idbarang}}" 
                                      data-qtybarangkirim = "{{$stspeng->qty_barang}}" 
                                      data-idstatus="{{$ds->idstatus_pengiriman}}"
                                      data-idstatus1="{{$dropdownstatus[6]->idstatus_pengiriman}}"
                                      href="#">{{$dropdownstatus[6]->status}}</a></li>
                                   <?php 
                                   break;
                                  } 
                                ?>
                                </ul>
                              </div>
                             <?php
                             } 
                             // Barang Complete
                             else if ($stspeng->status_pengiriman_idstatus_pengiriman== 6)
                             {
                             ?>
                             <div class="btn-group" >
                                <button type="button" class="btn btn-success"><?php echo $stspeng->status ?></button>
                                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                  <span class="caret"></span>
                                  <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                  <li><a class="showonlytablestatuscomplete" 
                                    data-idpengirimanstatuscomplete = "{{$stspeng->idpengiriman}}"
                                    data-idbarangstatuscomplete = "{{$stspeng->barang_idbarang}}"
                                    href="#">Histori Pengiriman</a></li>
                                </ul>
                                
                              </div>
                              <?php if ($stspeng->tgl_status_complete==null && $stspeng->penerima_status_complete==null){ ?>
                                <div class="btn-group" >
                                  <button type="button" id="statuscomplete" class="btn bg-blue showmodalstatuscomplete"  
                                  data-idpengcomplete = "{{$stspeng->idpengiriman}}"
                                  data-idbrgcomplete = "{{$stspeng->barang_idbarang}}"><?php echo $dropdownstatus[8]->status ?></button>
                                </div>
                              <?php }  else {?>
                                <div class="btn-group" >
                                  <button type="button" id="" class="btn bg-purple showmodalviewstatuscomplete"
                                  data-tglcomplete = "{{$stspeng->tgl_status_complete}}"
                                  data-penerimacomplete = "{{$stspeng->penerima_status_complete}}"
                                  >View Complete</button>
                                </div>
                              <?php } ?>
                           
                    
                             <?php
                             }
                              //Barang Dikirim Sebagian
                              else if ($stspeng->status_pengiriman_idstatus_pengiriman== 7)
                             {
                             ?>
                              <div class="btn-group">
                                <button type="button" class="btn btn-warning"><?php echo $stspeng->status ?></button>
                                <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                  <span class="caret"></span>
                                  <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                <?php 
                                // $first= true;
                                 foreach($dropdownstatus as $ds)
                                  {
                                    // hide first element
                                    // if ($first) { $first=false; continue; }
                                   ?>
                                    <li><a class="idstatusget"
                                      data-idpengiriman = "{{$stspeng->idpengiriman}}"
                                      data-idbarang = "{{$stspeng->barang_idbarang}}" 
                                      data-qtybarangkirim = "{{$stspeng->qty_barang}}" 
                                      data-idstatus="{{$ds->idstatus_pengiriman}}"
                                      data-idstatus1="{{$dropdownstatus[6]->idstatus_pengiriman}}"
                                      href="#">{{$dropdownstatus[5]->status}}</a></li>
                                   <?php
                                   break;  
                                  } 
                                ?>
                                </ul>
                              </div>
                              <?php 
                              }
                              ?>
                          </td>
                         </tr>
                        <?php  
                      }
                    ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Tanggal Trip</th>
                  <th>Kapal</th>
                  <th>Container</th>
                  <th>Pengirim</th>
                  <th>Penerima</th>
                  <th>Barang</th>
                  <th>Status Barang</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

        <!-- modal status tiba tujuan -->
        <div class="modal fade" id="popupstatustibatujuan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Nomor Bukti Tanda Terima</h4>
              </div>
                <div class="modal-body">
                  <form id="formupdatestatuspengiriman" method="POST" onsubmit="return validate(this);">
                    @csrf
                    <input type="text" id="idpengirimandalammodal" name="idpengiriman" hidden>
                    <input type="text" id="idbarangmodal" name="idbarangmodal" hidden>
                    
                 <div class="form-group">
                  <label>Tanggal Terima Barang</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right tglterimabarang" id="datepicker" name="datetglstatuskirim" required>
                    </div>
                  </div>
                  <label>Nama Penerima</label><br>
                  <input type="text" id="namapenerima" name="namapenerima" class="form-control" required> 
                  <label>No Transaksi Barang</label><br>
                  <input type="text" id="notransaksi" name="notransaksi" class="form-control" required> 
                  <label>Quantity Kirim</label><small>&nbsp;<i>(*qty barang yang tertera saat pengiriman barang)</i></small><br>
                  <input type="number" id="qtykirim" name="qtykirim" class="form-control changeqty" readonly> 
                  <label>Quantity Barang Yang Diterima</label><br>
                  <input type="number" min = "0" id="qtybrg" name="qtybrg" class="form-control changeqty" onkeypress="return isNumberKey(event)" required> 

                  <!-- validasi check barang diterima sebagian -->
                  <input type="text" id="qtybarangcheck" name="qtybarangcheck" class="changeqty" value="0" hidden>

                  <!-- validasi check jumlah barang yang diterima di db -->
                  <input type="text" id="qtyjumlahbrgdbsisa" name="qtyjumlahbrgdbsisa" value="0" hidden>
                  <!-- validasi check sisa barang diterima sebagian -->
                  <input type="text" id="qtysisacheck" name="qtysisacheck" class="changeqty" value = "0" hidden>

                  <label>Quantity Sisa Barang</label><br>
                  <input type="number" id="qtysisa" name="qtysisa" class="form-control" readonly> <br> <br>

                  <!-- hasil penguruangan qty dikirim sebagian db dan sisa yang belum dikrim -->
                  <input type="text" id="qtyakhirsebagian" name="qtyakhirkirimsebagian" hidden>

                  <!-- untuk if else di controller -->
                  <input type="text" id="qtybrgkirimsisa" name="qtybrgkirimsisacheck" hidden>
                  <label>History Pengiriman Barang</label>
                  <table id="tablehistoripengiriman" class="text-wrap table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No Transaksi</th>
                      <th>Tgl Terima</th>
                      <th>Penerima</th>
                      <th>Qty Terima</th>
                      <th>Sisa</th>
                    </tr>
                    </thead>
                    <!-- input via append -->
                    <tbody>
           
                    </tbody>
                  </table>

                  <button type="submit" class="btn bg-purple"  >Update</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                  </form>
                </div>
            </div>
          </div>
        </div>

        <!-- modal histori status tiba tujuan -->
        <div class="modal fade" id="popupstatuscomplete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Histori Pengiriman</h4>
              </div>
                <div class="modal-body">
                  <label>History Pengiriman Barang</label>
                  <table id="tablehistoricomplete" class="text-wrap table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No Transaksi</th>
                      <th>Tgl Terima</th>
                      <th>Penerima</th>
                      <th>Qty Terima</th>
                      <th>Sisa</th>
                    </tr>
                    </thead>
                    <!-- input via append -->
                    <tbody>
           
                    </tbody>
                  </table>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                </div>
            </div>
          </div>
        </div>

        <!-- modal complete -->
        <div class="modal fade" id="modalstatuscomplete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Complete</h4>
              </div>
                <div class="modal-body">
                  <form id="formstatuscomplete" method="POST" action="{{route('statuspengiriman.statuscomplete')}}">
                    @csrf
                    <div class="form-group">
                      <input type="text" id="idpengirimancomplete" name="idpengirimancomplete" hidden>
                      <input type="text" id="idbarangcomplete" name="idbarangcomplete" hidden>
                      <label>Tanggal </label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" id="tglmodalstatuscomplete" name="tglmodalstatuscomplete" required>
                        </div>

                       <label>Penerima </label>
                       <!-- /.box-header -->
                       <input type="text" class="form-control" id="idnamapenerimacomplete" name="namapenerimacomplete" required>
                    </div>
                  
                    <div id="divsubmitmodalcompleteshow">
                      <button type="submit" class="btn bg-purple">Submit</button> 
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                    </div>
                    <div id="divsubmitmodalcompletehide">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                    </div>
                  </form>
                </div>
            </div>
          </div>
        </div>
        <!-- modal spinner -->
      </section>

  </div>
@endsection
@section('script')
<!-- status complete  -->
<script type="text/javascript">

  $(".showmodalstatuscomplete").on("click", function(e){
    var idpengiriman = $(this).data("idpengcomplete");
    var idbarang = $(this).data("idbrgcomplete");

    $("#idpengirimancomplete").val(idpengiriman);
    $("#idbarangcomplete").val(idbarang);

    // set 0
    $("#idnamapenerimacomplete").val("");
    $("#tglmodalstatuscomplete").val("");

    document.getElementById("divsubmitmodalcompleteshow").style.display='block';
    document.getElementById("divsubmitmodalcompletehide").style.display='none';

    $("#modalstatuscomplete").modal("show");
  });

  $(".showmodalviewstatuscomplete").on("click", function(e){
    var tgl = $(this).data("tglcomplete");
    var penerima = $(this).data("penerimacomplete");

    $("#tglmodalstatuscomplete").val(tgl);
    $("#idnamapenerimacomplete").val(penerima);

    $("#idnamapenerimacomplete").attr("readonly", "readonly");
    $('#tglmodalstatuscomplete') .attr("disabled", "disabled");

    document.getElementById("divsubmitmodalcompletehide").style.display='block';
    document.getElementById("divsubmitmodalcompleteshow").style.display='none';
    
    $("#modalstatuscomplete").modal("show");
  });
  
</script>
<script type="text/javascript">
     // dropdown status 
      $('.idstatusget').on('click',function(e){
        //prevent windows jump up
        e.preventDefault();

        var idpengiriman = $(this).data('idpengiriman');
        var idbarang = $(this).data('idbarang'); 
        var idstatuspengiriman = $(this).data('idstatus'); 
        var idstatuspengiriman1 = $(this).data('idstatus1');// agar dropdown select 1 per 1
        var kapal = $(this).data('kapal');// status kirim jadi tiba sesuai nama kapal
        var container = $(this).data('container');// status kirim bongkar office cabang sesuai nama container

        var url = '{{route("statuspengiriman.update",":id")}}';
        
        url = url.replace(':id',idbarang);
        //alert(idpengiriman);

        //set id pengiriman dan id barang untuk form update status(modal)
        $("#idpengirimandalammodal").val(idpengiriman);
        $("#idbarangmodal").val(idbarang);


        if(idstatuspengiriman==6||idstatuspengiriman1==6){

            // set 0 input tag html after click
            $(".tglterimabarang").val("");
            $("#namapenerima").val("");
            $("#notransaksi").val("");
            $("#qtybrg").val("");
            $("#qtysisa").val(""); 
            $("#qtybarangcheck").val(0);
            $("#qtysisacheck").val(0);
            $('#qtybrg').val(0);
            $('#qtysisa').val(0);


            // append data table histori pengiriman sesuai dengan id pengiriman
             var urlhistori = "{{route('statuspengiriman.showhistoripengiriman')}}"
             $.ajaxSetup({
                        headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
             $.ajax({
              url: urlhistori,
              type:'POST',
              data:{idpengiriman,idbarang},
              }).done(function(response){

                var tr="";
                var sumbarangsisa = 0;
                $('#tablehistoripengiriman').dataTable().fnClearTable();
                   $.each(response, function (index, value) {
                     $("#tablehistoripengiriman").DataTable().row.add([
                          value.notransaksi, value.tgl_terima, value.nama_penerima, value.qty_barang_diterima, value.qty_sisa
                      ]).draw();
                     sumbarangsisa+=value.qty_barang_diterima;
                     // input to qty barang yang sudah diterima sebagian
                     $("#qtybrg").val(sumbarangsisa);
                     $("#qtyjumlahbrgdbsisa").val(sumbarangsisa);
                     
                     // qty sisa dari db
                     $("#qtysisa").val(value.qty_sisa);
                     // validasi barang diterima sebagian
                     $("#qtybarangcheck").val(value.qty_barang_diterima);
                     $("#qtysisacheck").val(value.qty_sisa);

                     // input no transaksi apabila sudah ada di db
                     $("#notransaksi").val(value.notransaksi);

                   });
               }); 
            //set data for qty barang and modal pop up
            var qtybarang = $(this).data('qtybarangkirim');
            $("#qtykirim").val(qtybarang);
            $("#popupstatustibatujuan").modal("show");
        }
        else
        {
          if (confirm("Ingin Update Data?")) {
            $.ajaxSetup({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
          $.ajax({
            url: url,
            type:'PUT',
            data:{idstatuspengiriman,idstatuspengiriman1,kapal,container},
            }).done(function(response){
              alert("Data Berhasil Diperbarui");
              location.reload();
           });  
          }
          else
          {
            return false;
          } 
        }

       });
        // show histori complete
        $('.showonlytablestatuscomplete').on('click',function(e){
        var idpengirimancomplete = $(this).data("idpengirimanstatuscomplete");
        var idbarangcomplete = $(this).data("idbarangstatuscomplete");

        var urlhistoricomplete = "{{route('statuspengiriman.showcomplete')}}"
         $.ajaxSetup({
                    headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
         $.ajax({
          url: urlhistoricomplete,
          type:'POST',
          data:{idpengirimancomplete,idbarangcomplete},
          }).done(function(response){
            var tr="";
            $('#tablehistoricomplete').dataTable().fnClearTable();
               $.each(response, function (index, value) {
                 $("#tablehistoricomplete").DataTable().row.add([
                      value.notransaksi, value.tgl_terima, value.nama_penerima, value.qty_barang_diterima, value.qty_sisa
                  ]).draw();
               });
           }); 
         $("#popupstatuscomplete").modal("show"); 
      }); 
</script>



<!-- table multiple search status pengiriman -->
<script type="text/javascript">
 $('#multiplesearchstatuspengiriman thead th').each( function () {
    var title = $('#multiplesearchstatuspengiriman tfoot th').eq( $(this).index() ).text();
    if(title!=""){
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
      }

    });

    // DataTable
    var table = $('#multiplesearchstatuspengiriman').DataTable({
    stateSave: true, //untuk pagination agar tidak terpindah ke first page setelah ajax reload
    scrollX : true,
    columnDefs: [
        { 
          searchable: false, targets: 6,
          width: 300, targets:  6
        },
        // date format
        {  render: function(data) {
            return moment(data).format('DD-MM-YYYY');
          }, "targets": 0
        }
    ],
    'select': {
         'style': 'multi'
      },
    'order': [[1, 'asc']],



    
    //  //apply every page(dipindah keatas biar nda loop2)
    // 'drawCallback': function(settings)
    //   {
     
    //  }
    });
    // Restore state after page refresh
    var state = table.state.loaded();
    if (state) {
        table.columns().eq(0).each(function (colIdx) {
            var colSearch = state.columns[colIdx].search;

            if (colSearch.search) {
                $('input', table.column(colIdx).header()).val(colSearch.search);
            }
        });

        table.draw();
    }
    // Apply the search
    table.columns().eq( 0 ).each( function ( colIdx ) {
        
        if( !table.settings()[0].aoColumns[colIdx].bSearchable ){
        table.column( colIdx ).header().innerHTML=table.column( colIdx ).footer().innerHTML;
    }
        $( 'input', table.column( colIdx ).header() ).on( 'keyup change', function () {
          // make position relative after search
          $(".dropdown-menu").attr("style","position:relative;");
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        });
    });
</script>


<!-- validasi untuk input qty barang diterima sebagian -->
<script type="text/javascript">
 function validate(form) {
  // get qty db untuk mendapatkan sisa sebagian
  var qtyawal = $("#qtykirim").val();
  // untuk validasi apabila barang di database tidak sama dengan yang mau di input maka insert sisanya(sebagian)
  var qtybarangditerima = $("#qtybrg").val();
  // get data dar db table barang_terkirim untuk get sisa
  var qtybrgkirimsisacheck = $("#qtybarangcheck").val();
  // sum dikirim sebagian dari db
  var qtyjumlahbrgdbsisa = $("#qtyjumlahbrgdbsisa").val();
  var qtyakhirkirimsebagian = qtyawal-qtybrgkirimsisacheck;

  // set qty akhir kirim sebagian
  $("#qtyakhirsebagian").val(qtyakhirkirimsebagian);

  // set sisa qty akhir dikirim sebagian untuk if else di controller
  $("#qtybrgkirimsisa").val(qtybrgkirimsisacheck);

      if(+qtybarangditerima!=+qtybrgkirimsisacheck) 
      {
         if(+qtybarangditerima<+qtyjumlahbrgdbsisa)
          {
          alert("Qty barang yang diterima dibawah dari Qty dikirim sebagian");
          // value dari DB
          $('#qtybrg').val(qtyjumlahbrgdbsisa);
          return false;
          }
          return confirm('Ingin Update Status Pengiriman?');

          // jika return yes
          var urlupdatestatus = '{{route("statuspengiriman.store")}}';
          var data = new FormData($('#formupdatestatuspengiriman')[0]);
          $.ajaxSetup({
              headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
            $.ajax({
              url: urlupdatestatus,
              type:'POST',
              data:data,
              });
          
      }
     
      else 
      {
          alert("Ubah Qty barang yang akan dikirim");
          return false;
      }
    }
</script>

<!-- input qty barang sisa setelah kirim -->
<script type="text/javascript">
$(document).on('input', '.changeqty', function()
{
   var qtykirim = $('#qtykirim').val();
   var qtybarang = $('#qtybrg').val();
   // sum dikirim sebagian dari db
   var qtyjumlahbrgdbsisa = $("#qtyjumlahbrgdbsisa").val();
   // valdiasi sisa barang diterima sebagian
   var qtysisasebagiancheck = $('#qtysisacheck').val();

   var sisa = qtykirim-qtybarang;
   
   // unary plus convert to number
   if(+qtybarang<+qtyjumlahbrgdbsisa)
   {
    if (qtybarang=="") {
      return true;
    }
    // alert check qty barang diterima<qtyjumlah diterima sebagian ada di function validate
     $('#qtysisa').val(qtysisasebagiancheck);
   }
   else if(+sisa<0)
   {
     alert("Qty barang terima melebihi Qty barang kirim");
     $('#qtybrg').val(qtyjumlahbrgdbsisa);
     $('#qtysisa').val(qtysisasebagiancheck); 
   }
   else
   {
     $('#qtysisa').val(sisa); 
   }

   
});
</script>

<!-- for input number only -->
<script type="text/javascript">
  function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}    
</script>

<script type="text/javascript">
$(function () {
    $('#tablehistoripengiriman').DataTable();
});
</script>

<script type="text/javascript">
$(function () {
    $('#tablehistoricomplete').DataTable();
});
</script>

@endsection

