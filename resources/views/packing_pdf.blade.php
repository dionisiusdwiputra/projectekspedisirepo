
<!DOCTYPE html>
<html>

<head>
  <title>PDF Kapal</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style type="text/css">
  .page_break{page-break-before:always;}

  #footer {
      position: fixed;
      right: 0px;
      bottom: 10px;
      text-align: center;
      border-top: 1px solid black;
    }

  #footer .page:after{
    content: counter{page,decimal};
  }

  @page {
      margin: 20px 30px 40px 50px;
    }
</style>
</head>
<body>
  <style type="text/css">
    table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  width: 100%;
}
  </style>
  <center>
   <?php $d=$tableprint[0];?>
   <h4>DAFTAR ISI KAPAL <?php echo $d->kapal ?></h4>
    <h6><a target="_blank" href="">KEBERANGKATAN : <?php echo $d->kota_pengirim ?>, TUJUAN <?php echo $d->kota_penerima ?>, TRIP : <?php echo $d->tgl_pengiriman  ?></a></h5>
  </center>
  <div>
  <table>
    <thead>
      <tr>
        <th>Nama Container</th>
        <th>Nama Penerima</th>
        <th>Barang</th>
        <th>Qty Barang</th>
        <th>Total Kubikasi</th>
      </tr>
    </thead>
    <tbody>
        <?php
         $index =0;
         $totalkubikasiseluruhnya = 0;
         $totalqtybarang=0;
         foreach($tableprint as $d=>$data)
                    {
                      $idpengirimanutkrowspan = $tableprint->where('idpengiriman',$data->idpengiriman)->count();
                       $index+=1;
                      ?>
                        <tr>
                          <?php if($index==1) { ?>
                             <td rowspan="{{$idpengirimanutkrowspan}}">
                              <?php
                                echo $data->nama_container." / ".$data->seal;
                              ?>
                            </td>
                            <?php } ?>
                            
                          <?php if($index==1){ ?>
                            <td rowspan="{{$idpengirimanutkrowspan}}">
                              <?php
                                echo $data->nama_penerima;
                              ?>
                            </td>
                            <?php } ?>
                            <td>
                              <?php
                                echo $data->nama_barang;
                              ?>
                            </td>
                            <td>
                              <?php 
                            	echo $data->qty_barang;
                            	$totalqtybarang+=$data->qty_barang;
                              ?>
                            </td>
                            <td>
                              <?php
                                echo $data->total_kubikasi;
                                $totalkubikasiseluruhnya+=$data->total_kubikasi;
                              ?>
                            </td>
                           </tr>
                        <?php 
                        if ($index==$idpengirimanutkrowspan) 
                        { 
                        	$index=0;
                        	?>
                         <tr>
                        	<td></td>
					        <td></td>
					        <td><b style="color:#1788f4;"><i>Sub Total</i></b></td>
					        <td><b style="color:#1788f4;"><i><?php echo $totalqtybarang; $totalqtybarang=0;?></i></b></td>
					        <td><b style="color:#1788f4;"><i><?php echo $totalkubikasiseluruhnya; $totalkubikasiseluruhnya = 0;?></i></b></td>
                        </tr> 	
                        <?php 
                        }?>
                        
                      <?php
                    }
                  ?>
      </tbody>
      <tfoot>
        <tr>
	        <td></td>
	        <td></td>
	        <td><b style="color:#ff4646"><i>Total</i></b></td>
	        <td><b style="color:#ff4646"><i><?php $qty=0; foreach($tableprint as $d=>$data){$qty+=$data->qty_barang; } echo $qty; ?></i></b></td>
	        <td><b style="color:#ff4646"><i><?php $kubikasi=0; foreach($tableprint as $d=>$data){$kubikasi+=$data->total_kubikasi; } echo $kubikasi; ?></i></b></td>
        </tr>
      </tfoot>
    </table>
  </div>
<!-- halaman -->
  <div id="footer">
    <p class="page">Page</p>
  </div>
<!-- next page -->
  <div class="page_break"></div>
<!-- content page baru -->
  <div>
      <center><h4>KETERANGAN DRIVER</h4></center> <br>
      <div>

      <p>
        <?php 
        foreach ($tableprint as $key => $value) {
          echo $value->keterangan_driver;
          break;
        }
         ?>
      </p>
      </div>
  </div>
  <!-- halaman -->
  <div id="footer">
    <p class="page">Page</p>
  </div>
<!-- next page -->
  <div class="page_break"></div>
  <div>
     <center><h4>KETERANGAN OFFICE</h4></center>
     <div>
       <p><?php 
        foreach ($tableprint as $key => $value) {
          echo $value->keterangan_office;
          break;
        }
         ?></p>
     </div>
  </div>
  <div id="footer">
    <p class="page">Page</p>
  </div>
</body>
</html>
