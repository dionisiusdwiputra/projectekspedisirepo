@extends('layouts.layout')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
        Pengiriman
        <!-- <small>Control panel</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Pengiriman</li>
      </ol>
   </section>
   <section class ="content">
  	  <div class="row">
        <div class="col-xs-12">
        <form role="form" id="formdeletepengiriman">
      	@csrf
      	@method('POST')

        @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong>{{ $message }}</strong>
          </div>
        @endif

        @if ($message = Session::get('error'))
          <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
        @endif

        @if ($message = Session::get('warning'))
          <div class="alert alert-warning alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
        @endif

        @if ($message = Session::get('info'))
          <div class="alert alert-info alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
        @endif

        @if ($errors->any())
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            Please check the form below for errors
        </div>
        @endif
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Data Pengiriman</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <input type="hidden" id="idpengirimandeletehide" name="idpengirimandeletehide">
              <input type="hidden" id="idbarangdeletehide" name="idbarangdeletehide">
<!--          <input type="hidden" id="idpengirimdeletehide" name="idpengirimdeletehide">
              <input type="hidden" id="idpenerimadeletehide" name="idpenerimadeletehide"> -->

              <!-- keperluan untuk menambah total kubikasi setelah delete pengiriman -->
              <input type="hidden" id="sisacontainertemp" name="sisacontainertemp">
              <input type="hidden" id="namacontaineruntukdelete" name="namacontaineruntukdelete"> 
            <table id="example1" class="text-wrap table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Kapal</th>
                  <th>Tanggal Trip</th>
                  <th>Container</th>
                  <th>Pengirim</th>
                  <th>Penerima</th>
                  <th>Barang</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 	<?php 
                      for ($i=0; $i <count($pengiriman) ; $i++) 
                      { 
                          $listpengiriman=$pengiriman[$i];
                        ?>   
                        <tr>
                     		<td>
                             <?php 
                                 echo $listpengiriman->kapal;
                              ?>
                       		</td>
                       		<td>
                             <?php 
                                 echo $listpengiriman->tgl_pengiriman;
                              ?>
                       		</td>
                       		<td>
                             <?php 
                                 echo $listpengiriman->nama_container;
                              ?>
                       		</td>
                       		<td>
                             <?php 
                                 echo $listpengiriman->nama_pengirim;
                              ?>
                       		</td>
                       		<td>
                             <?php 
                                 echo $listpengiriman->nama_penerima;
                              ?>
                       		</td>
                       		<td>
                             <?php 
                                 echo $listpengiriman->nama_barang;
                              ?>
                       		</td>
                       		<td>
                       			<!-- update -->
                       			<button id="edit" type="button" name="edit" class="btn btn-warning editpengiriman" data-toggle="modal" data-target="#editpengiriman"
                       			data-idpengiriman = "{{$listpengiriman->idpengiriman}}"
                       			data-idbarang = "{{$listpengiriman->idbarang}}"
                       			data-idpengirim = "{{$listpengiriman->pengirim_idpengirim}}"
                       			data-idpenerima = "{{$listpengiriman->penerima_idpenerima}}"
                       			data-kapal = "{{$listpengiriman->kapal}}"
                       			data-tglpengiriman = "{{$listpengiriman->tgl_pengiriman}}"
                       			data-container = "{{$listpengiriman->nama_container}}"
                       			data-pengirim = "{{$listpengiriman->nama_pengirim}}"
                       			data-penerima = "{{$listpengiriman->nama_penerima}}"
                       			data-qty = "{{$listpengiriman->qty_barang}}"
                       			data-panjang = "{{$listpengiriman->panjang}}"
                       			data-lebar = "{{$listpengiriman->lebar}}"
                       			data-tinggi = "{{$listpengiriman->tinggi}}"
                       			data-totalkubikasi = "{{$listpengiriman->total_kubikasi}}"
                       			data-barang = "{{$listpengiriman->nama_barang}}"
                            data-sisacontainer = "{{$listpengiriman->sisa_container_temp}}"
                       			><div class="fa fa-pencil-square-o"></div></button>

                       			<!-- delete -->
                       			<button id="delete" type="button" name="delete" class="btn btn-danger deletepengiriman"  
                       			data-idpengirimandelete = "{{$listpengiriman->idpengiriman}}"
                       			data-idbarangdelete = "{{$listpengiriman->idbarang}}"
                            data-idpengirimdelete = "{{$listpengiriman->pengirim_idpengirim}}"
                            data-idpenerimadelete = "{{$listpengiriman->penerima_idpenerima}}"
                            data-totalkubikasi = "{{$listpengiriman->total_kubikasi}}"
                            data-sisacontainer = "{{$listpengiriman->sisa_container_temp}}"
                            data-container = "{{$listpengiriman->nama_container}}"
                       			><div class="fa fa-trash"></div></button>
                       		</td>
                         </tr>
                        <?php  
                      }
                    ?>
                </tbody>
               </table>
       

            </div>
            <!-- /.box-body -->
            <!-- Loading (remove the following to stop the loading)-->
            <!-- <div class="overlay loadingpengiriman">
              <i class="fa fa-refresh fa-spin"></i>
            </div> -->
            <!-- end loading -->
          </div>
          <!-- /.box -->
          </form>
        </div>
        <!-- /.col -->
      </div>
 
      <form role="form" method="POST" action="{{route('pengiriman.store')}}" onsubmit="return validate(this)" >
      	@csrf
       <div class="box box-primary">
       	
        <div class="box-header with-border">
          <h3 class="box-title">Detail Pengiriman</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
         <div style="width: 100%; overflow: auto;">
   			 	<div style="width: 49%; float: left;"> 	
   			 		<h4 class="control-label"><b>Pengirim</b></h4>
   			 		 <select class="form-control select2" style="width: 100%;" id="idpengirim" name="idpengirim">
	                  <option value="0">Tambah Pengirim</option>
		                   <?php 
		                      for ($i=0; $i <count($listpengirim) ; $i++) 
		                      { 
		                          $list=$listpengirim[$i];
		                        ?>   

		                           <option value="{{$list->idpengirim}}" 
		                           	data-namapengirim="{{$list->nama_pengirim}}"
		                           	data-alamatpengirim="{{$list->alamat_pengirim}}"
		                           	data-kotapengirim="{{$list->kota_pengirim}}"
		                           	data-notelppengirim="{{$list->no_telp_pengirim}}">
		                             <?php 
		                                 echo $list->nama_pengirim;
		                              ?>
		                           </option>
		                        <?php  

		                      }
		                    ?>
	                </select> <br><br>
	                <small>Nama Pengirim</small><br>
	                <input type="text" id="namapengiriminsert" name="namapengiriminsert" class="form-control" required>
	                <small>Alamat Pengirim</small><br>
	                <input type="text" id="alamatpengiriminsert" name="alamatpengiriminsert" class="form-control" required> 
	                <small>Kota Pengirim</small><br>
	                <input type="text" id="kotapengiriminsert" name="kotapengiriminsert" class="form-control" required> 
	                <small>Nomer Telp Pengirim</small><br>
	                <input type="text" id="notelppengiriminsert" name="notelppengiriminsert" class="form-control" onkeypress="return isNumberKey(event)" required> 
	            </div>

    			<div style="margin-left: 50%;">
	    			<h4 class="control-label"><b>Penerima</b></h4>
   			 		 <select class="form-control select2" style="width: 100%;" id="idpenerima" name="idpenerima">
	                  <option value="0">Tambah Penerima</option>
	                   <?php 
		                      for ($i=0; $i <count($listpenerima) ; $i++) 
		                      { 
		                          $list=$listpenerima[$i];
		                        ?>   
		                           <option value="{{$list->idpenerima}}"
		                           	data-namapenerima="{{$list->nama_penerima}}"
		                           	data-alamatpenerima="{{$list->alamat_penerima}}"
		                           	data-kotapenerima="{{$list->kota_penerima}}"
		                           	data-notelppenerima="{{$list->no_telp_penerima}}">
		                             <?php 
		                                 echo $list->nama_penerima;
		                              ?>
		                           </option>
		                        <?php  

		                      }
		                    ?>
	                </select> <br><br>
	                <small>Nama Penerima</small><br>
	                <input type="text" id="namapenerimainsert" name="namapenerimainsert" class="form-control" required> 
	                <small>Alamat Penerima</small><br>
	                <input type="text" id="alamatpenerimainsert" name="alamatpenerimainsert" class="form-control" required> 
	                <small>Kota Tujuan</small><br>
	                <input type="text" id="kotapenerimainsert" name="kotapenerimainsert" class="form-control" required> 
	                <small>Nomer Telp Penerima</small><br>
	                <input type="text" id="notelppenerimainsert" name="notelppenerimainsert" class="form-control" onkeypress="return isNumberKey(event)" required>
    			</div>
			</div> <br><br>
			<div style="width: 100%; overflow: auto;">
   			 <div style="width:49%; float: left;"> 	
        
          <!-- input tgl Trip -->
            <label>Tanggal Trip Kapal</label>
            <div class="input-group date">
              <div class="input-group-addon disabledpopup">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" class="form-control pull-right datetglkapal" id="datepicker" name="datetglkapal" >

            </div>
            
            <!-- list kapal -->
            <label>List Kapal</label>
             <select class="form-control select2" style="width: 100%;" id="listkapal">
              <option value=" ">Pilih List Kapal / Tambah Kapal Baru</option>
               <?php 
                  for ($i=0; $i <count($namakapaltemp) ; $i++) 
                  { 
                      $kk=$namakapaltemp[$i];
                    ?>   
                       <option value="{{$kk->kapal_temp}}"
                        data-tglkapallist="{{$kk->tgl_pengiriman_temp}}"
                        >
                         <?php 
                             echo $kk->kapal_temp;
                          ?>
                       </option>
                    <?php  

                  }
                ?>
              </select>
            <!-- input kapal -->
          <label class="control-label">Kapal</label><br>
          <input type="text" id="namakapal" name="namakapal" class="form-control" required> 
	        </div>

    			<div style="margin-left: 50%;">
            <!-- jenis container -->
    		    <label>Jenis Container</label>
             <select class="form-control" style="width: 100%;" id="inputcontainer" name="inputcontainer" required>
              <option value="">Pilih Jenis Container</option>
               <?php 
                  for ($i=0; $i <count($listcontainer) ; $i++) 
                  { 
                      $list=$listcontainer[$i];
                    ?>   
                       <option value="{{$list->idcontainer}}" 
                       	data-id="{{$list->idcontainer}}"
                       	data-minimum="{{$list->batas_minimum}}"
                        >
                         <?php 
                             echo $list->nama_jenis_container;
                          ?>
                       </option>
                    <?php  

                  }
                ?>
             </select>
            <!-- list container -->
            <label>List Container</label>
            <input type="text" id="idjenisconttemp" name="idjenisconttemp" hidden>
             <select class="form-control select2" style="width: 100%; " id="listcontainer">
              <option value=" ">Pilih List Container / Tambah Container Baru</option>
               <?php 
                  for ($i=0; $i <count($kontainerkapal) ; $i++) 
                  { 
                      $kk=$kontainerkapal[$i];
                    ?>   
                       <option value="{{$kk->nama_container_temp}}" 
                        data-idcontainertemp="{{$kk->jenis_container_idcontainer}}"
                        data-sisacontainer="{{$kk->sisa_container_temp}}"
                        data-jeniscontainer="{{$kk->batas_minimum}}"
                        data-tgllistcontainer="{{$kk->tgl_pengiriman_temp}}"
                        data-namakapalselectedcontainer="{{$kk->kapal_temp}}"
                        >
                         <?php 
                             echo $kk->nama_container_temp;
                          ?>
                       </option>
                    <?php  

                  }
                ?>
              </select>
            <!-- input container -->
		    		<label class="control-label">Container</label><br>
            <input type="text" id="namacontainer" name="namacontainer" class="form-control" required> 
    			</div>
        </div> 
        <label>Jenis Pengiriman</label>
            <select class="form-control" style="width: 100%;" id="inputpengiriman" name="inputpengiriman" required>
              <option value="">Pilih Jenis Pengiriman</option>
               <?php 
                  for ($i=0; $i <count($listjenispengiriman) ; $i++) 
                  { 
                      $list=$listjenispengiriman[$i];
                    ?>   
                       <option value="{{$list->idjenis_pengiriman}}">
                         <?php 
                             echo $list->nama_jenis_pengiriman;
                          ?>
                       </option>
                    <?php  

                  }
                ?>
            </select>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      	<div class="row">
      	 <div class="col-xs-12">
      	  <div class="box box-primary">
            <div class="box-header">
             <small>Teru Tritunggal | Pengiriman</small> 
             <!-- data-target="#tambahitem" data-toggle="modal" -->
              <button type="#" id="tambahbarang" class="btn btn-success edit"  style="float: right" >+ Tambah barang</button><br><br>
              <div class="box-body" id="container">
              <table id="tablebarangpengiriman" class="table table-bordered table-striped perhitungan">
                <thead>
                <tr>
                  <th>Nama Barang</th>
                  <th>Qty(coly)</th>
                  <th>Tonase/Meter</th>
                  <th>P (Meter)</th>
                  <th>L (Meter)</th>
                  <th>T (Meter)</th>
                  <th>Kubikasi (M <sup>3</sup>)</th>
                  <th>action</th>
                </tr>
                </thead>
                <tbody id="bodybarang" >
                	<tr>
	                	<td><input type="text" id="namabarang-0" name="barang[0][namabarang]" data-idx="0" required> </td>
	                	<td><input type="number" id="qty-0" name="barang[0][qty]" class="input-change" data-idx="0" onkeypress="return isNumberKey(event);" required></td>
                    <td> <select class="form-control" id="tonasemeter-0" name="barang[0][tonasemeter]" data-idx="0"> <option value="Tonase">Tonase</option><option value="Meter" required>Meter</option></select></td>
	                	<td><input type="number" step='0.001' value='0.000' placeholder='0.000' id="panjang-0" name="barang[0][panjang]" class="input-change" data-idx="0" required ></td>
	                	<td><input type="number" step='0.001' value='0.000' placeholder='0.000' id="lebar-0" name="barang[0][lebar]"  class="input-change" data-idx="0" required></td>
	                	<td><input type="number" step='0.001' value='0.000' placeholder='0.000' id="tinggi-0" name="barang[0][tinggi]"  class="input-change" data-idx="0" required></td>
	                	<td><input type="text" id="hasil-0" value="0.000" name="barang[0][hasil]" class="input-change" data-idx="0"  readonly="readonly"></td>
	                	<td></td>
                	</tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td style="text-align: right;"><b>TOTAL QTY</b></td>
                    <td><input type="text" id="totalqtycoly" name="totalqtycoly" disabled="disabled" ></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="text-align: right;"><b>TOTAL</b></td>
                    <td><input type="text" id="totalkubikasi" name="totalkubikasi" disabled="disabled" ></td>
                    <td></td>
                  </tr>
                </tfoot>
              </table>
              <input type="text" id="batasminimhide" name="batasminimhide" hidden>
              <input type="text" id="sisahide" name="sisahide" hidden>
              <input type="text" id="sisatampilupdatedb" name="sisatampilupdatedb" hidden>
              <h4 style="color:blue;">Batas Maksimal = <input type="text" class="btsminimtampil" id="btsminimtampil" style="border:0px;" readonly></h4>
              <h5 style="color:green;">Sisa = <input type="text" class="sisatampil" id="sisatampil" style="border:0px;" readonly></h5>
            </div>
           <button  type="submit" class="btn btn-primary" >Tambah Packing List</button> 
           <!-- <button  type="button" class="btn btn-danger prosespengiriman" >Proses Pengiriman</button> -->
          </div>
      	</div>
      </div>
     </div>
    </form>
   </section>
   <!-- endsection -->
   <div class="modal fade" id="editpengiriman" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Edit Data Pengiriman</h4>
              </div>
                <form role="form" method="PUT" id="updatepengirimanmodal">
                  @csrf
                  @method('PUT')
                  <div class="modal-body">
                  <input type="hidden" id="idpengirimanupdate" name="idpengirimanupdate">
	                <input type="hidden" id="idbarangupdate" name="idbarangupdate">
	                <input type="hidden" id="idpengirimupdate" name="idpengirimupdate">
	                <input type="hidden" id="idpenerimaupdate" name="idpenerimaupdate">
                  <!-- <label>List Kapal</label>
                   <select class="form-control select2" style="width: 100%;" id="updatekapaledit">
                    <option value=" ">Pilih List Kapal</option>
                     <?php 
                        for ($i=0; $i <count($namakapaltemp) ; $i++) 
                        { 
                            $kk=$namakapaltemp[$i];
                          ?>   
                             <option value="{{$kk->kapal_temp}}">
                               <?php 
                                   echo $kk->kapal_temp;
                                ?>
                             </option>
                          <?php  

                        }
                      ?>
                    </select> -->
                  <label  class="control-label">Kapal</label>
	                <input type="text" id="kapalupdate" name="kapalupdate" class="form-control" readonly> 
	                <div class="form-group">
		                <label>Tanggal Trip</label>
		                <div class="input-group date">
		                  <div class="input-group-addon">
		                    <i class="fa fa-calendar"></i>
		                  </div>
		                  <input type="text" class="form-control pull-right" id="datepickereditpengiriman" name="datetgleditkapalupdate" >
		                </div>
	                </div>
	                <label  class="control-label">Container</label>
	                <input type="text" id="containerupdate" name="containerupdate" class="form-control" readonly> 
	                <label  class="control-label">Pengirim</label>
	                <input type="text" id="pengirimupdate" name="pengirimupdate" class="form-control"> 
	                <label  class="control-label">Penerima</label>
	                <input type="text" id="penerimaupdate" name="penerimaupdate" class="form-control"> 
	                <label  class="control-label">Barang</label>
	                <input type="text" id="barangupdate" name="barangupdate" class="form-control">  
	                <label  class="control-label">Qty</label>
	                <input type="number" id="qtyupdate" name="qtyupdate" class="form-control change">  
	                <label  class="control-label">Panjang</label>
	                <input type="number"step='0.001' value='0.000' placeholder='0.000' id="panjangupdate" name="panjangupdate" class="form-control change">  
	                <label  class="control-label">Lebar</label>
	                <input type="number"step='0.001' value='0.000' placeholder='0.000' id="lebarupdate" name="lebarupdate" class="form-control change">  
	                <label  class="control-label">Tinggi</label>
	                <input type="number"step='0.001' value='0.000' placeholder='0.000' id="tinggiupdate" name="tinggiupdate" class="form-control change">  
	                <label  class="control-label">Total Kubikasi</label>
	                <input type="number" id="totalkubikasiupdate" name="totalkubikasiupdate" class="form-control change" readonly>
                  <input type="text" id="totalkubikasiupdatehide" name="totalkubikasiupdatehide" hidden> <br>

                  <label  class="control-label">Sisa Container</label> 
                  <input type="text" id="sisatampiledithide" name="sisatampiledithide" hidden><!-- input hidden untuk penjumlahan sisa tampil-->
                  <h4 style="color:green;">Sisa = <input type="text"value='0.000' name="sisatampiledit" id="sisatampiledit" style="border:0px;"></h4>
                  </div>
                 <div class="modal-footer">
                      <button type="button" id="updatedatapengiriman" name="updatedatapengiriman" class="btn btn-warning update" >Update Data</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                 </div>
                
               </form>   
            </div>
          </div>
        </div>
 </div>
@endsection
@section('script')
<!-- input just number -->
<script type="text/javascript">
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}    
</script>

<script type="text/javascript">
var scntDiv = $('#bodybarang');
var i = $('#bodybarang tr').length + 1;
var loop = 0;

$('#tambahbarang').click(function() {
	loop ++;
    scntDiv.append(`
    	<tr>
			<td><input type="text" id="namabarang-`+loop+`" name="barang[`+loop+`][namabarang]" data-idx="`+loop+`" required><input type="text" id="idloopdelete" name="idloopdelete" value="`+loop+`" hidden ></td>
        	<td><input type="number" id="qty-`+loop+`" name="barang[`+loop+`][qty]" class="input-change" data-idx="`+loop+`" required></td>
          <td><select class="form-control" id="tonasemeter-`+loop+`" name="barang[`+loop+`][tonasemeter]" data-idx="`+loop+`"> <option value="Tonase">Tonase</option><option value="Meter">Meter</option></select></td>
        	<td><input type="number" step='0.001' value='0.000' placeholder='0.000' id="panjang-`+loop+`" name="barang[`+loop+`][panjang]" class="input-change" data-idx="`+loop+`" required></td>
        	<td><input type="number" step='0.001' value='0.000' placeholder='0.000' id="lebar-`+loop+`" name="barang[`+loop+`][lebar]"  class="input-change" data-idx="`+loop+`" required></td>
        	<td><input type="number" step='0.001' value='0.000' placeholder='0.000' id="tinggi-`+loop+`" name="barang[`+loop+`][tinggi]"  class="input-change" data-idx="`+loop+`" required></td>
        	<td><input type="text" id="hasil-`+loop+`" name="barang[`+loop+`][hasil]" class="input-change" data-idx="`+loop+`" readonly="readonly"></td>
        	<td><button type="button" id="deleteitem" class="btn btn-danger delete" style="display: block; margin: auto;""><i class="fa fa-trash"></i></button></td>	
    	</tr>
    	`);   
    i++;
    return false;
});

$(document).on('input', '.input-change', function()
{
   var selectedlistcont = $("#listcontainer").val();
   var totalpenjumlahankubikasi =0;
   var totalpenjumlahanqty = 0;
   var idx = $(this).data('idx');
   var qty = $('#qty-' + idx).val();
   var panjang = $('#panjang-'+idx).val();
   var lebar =  $('#lebar-'+idx).val();
   var tinggi =  $('#tinggi-'+idx).val();
   var volume = qty*(panjang*lebar*tinggi);
   var decimal = volume.toFixed(2);

   $('#hasil-'+idx).val(decimal);

   var i;
   for(i=0; i<=idx; i++)
   {

    var totalkubikasi = parseFloat($('#hasil-'+i).val()) || 0;
    var totalqty = parseFloat($('#qty-'+i).val()) || 0;


    totalpenjumlahankubikasi+=totalkubikasi;
    totalpenjumlahanqty+=totalqty;
   }

   // IF ELSE untuk selected list container yang ada di db temp
   if (selectedlistcont==" ") {
    var sisa = $("#btsminimtampil").val()-totalpenjumlahankubikasi;
   }
   else
   {
    // acuan pada input sisahide untuk pengurangan
    var sisa = $("#sisahide").val()-totalpenjumlahankubikasi;
   }
   
   var decimaltotal = totalpenjumlahankubikasi.toFixed(2);
   // total kubikasi
   $('#totalkubikasi').val(decimaltotal);

   // total qty/coly
   $("#totalqtycoly").val(totalpenjumlahanqty);

   //untuk di tampilkan
   $('#sisatampil').val(sisa);

   //untuk di update di table jenis_container update
   $('#sisatampilupdatedb').val(sisa);
   
   

   //$('#btsminim').val($("#batasminimum").val()+' ('+sisa+')');


   
});

//remove
$(document).on('click', '.delete', function() {
  var id = $(this).closest('tr').find('#idloopdelete').val();

  var totalkubikasidelete= $("#totalkubikasi").val();
  var totalqtydelete = $("#totalqtycoly").val();

  
  var qty = $("#qty-"+id).val();
  var kubikasi = $("#hasil-"+id).val();


  var kubikasiafterdelete = totalkubikasidelete-kubikasi;
  var qtyafterdelete = totalqtydelete-qty;

  // total kubikasi after delete
   $('#totalkubikasi').val(kubikasiafterdelete);

   // total qty/coly after delete
   $("#totalqtycoly").val(qtyafterdelete);

    if (i > 2) {
        $(this).closest('tr').remove();
        $(this).parent().parent().remove();
        i--;
    }
    return false;

});
</script>

<!-- list pengirim -->
<script type="text/javascript">
	$(document).ready(function(){
    $('#idpengirim').on('change', function() {
    	if ($(this).val() > 0) 
    	  {
			 $("#namapengiriminsert").attr("readonly", "readonly");
			 $("#alamatpengiriminsert").attr("readonly", "readonly");
			 $("#kotapengiriminsert").attr("readonly", "readonly");
			 $("#notelppengiriminsert").attr("readonly", "readonly");
		  } 
		  else 
		  {
			 $("#namapengiriminsert").removeAttr("readonly");
			 $("#alamatpengiriminsert").removeAttr("readonly");
			 $("#kotapengiriminsert").removeAttr("readonly");
			 $("#notelppengiriminsert").removeAttr("readonly");
		  }

    		var nama = $(this).find(':selected').data('namapengirim');
    		var alamat = $(this).find(':selected').data('alamatpengirim');
    		var kota = $(this).find(':selected').data('kotapengirim');
    		var notelp = $(this).find(':selected').data('notelppengirim');

    		$("#namapengiriminsert").val(nama);
        $("#alamatpengiriminsert").val(alamat);
        $("#kotapengiriminsert").val(kota);
        $("#notelppengiriminsert").val(notelp);
    }); 
});
</script>

<!-- list penerima -->
<script type="text/javascript">
	$(document).ready(function(){
    $('#idpenerima').on('change', function() {
    	 if ($(this).val() > 0) 
    	  {
			 $("#namapenerimainsert").attr("readonly", "readonly");
			 $("#alamatpenerimainsert").attr("readonly", "readonly");
			 $("#kotapenerimainsert").attr("readonly", "readonly");
			 $("#notelppenerimainsert").attr("readonly", "readonly");
		  } 
		  else 
		  {
			 $("#namapenerimainsert").removeAttr("readonly");
			 $("#alamatpenerimainsert").removeAttr("readonly");
			 $("#kotapenerimainsert").removeAttr("readonly");
			 $("#notelppenerimainsert").removeAttr("readonly");
		  }

    		var nama = $(this).find(':selected').data('namapenerima');
    		var alamat = $(this).find(':selected').data('alamatpenerima');
    		var kota = $(this).find(':selected').data('kotapenerima');
    		var notelp = $(this).find(':selected').data('notelppenerima');

    		$("#namapenerimainsert").val(nama);
        $("#alamatpenerimainsert").val(alamat);
        $("#kotapenerimainsert").val(kota);
        $("#notelppenerimainsert").val(notelp);
     }); 
});
</script>

<!-- list kapal -->
<script type="text/javascript">
  $(document).ready(function(){
    $('#listkapal').on('change', function() {
      
       if ($(this).val() !=" ") 
      {
       $("#namakapal").attr("readonly", "readonly");
       $('.datetglkapal').prop('readonly', true);
     } 
      else 
      {
       $("#namakapal").removeAttr("readonly");
       $('.datetglkapal').prop('readonly', false);
      }

      var e = document.getElementById("listkapal");
      var namakapal = e.options[e.selectedIndex].value;
      var tglkapallist = $(this).find(':selected').data('tglkapallist');
     
      $("#namakapal").val(namakapal);
      $(".datetglkapal").val(tglkapallist);
     
     }); 
});
</script>

<!-- list kapal untuk edit-->
<script type="text/javascript">
  $(document).ready(function(){
    $('#listkapaledit').on('change', function() {

      var e = document.getElementById("listkapaledit");
      var namakapal = e.options[e.selectedIndex].value;

      $("#kapalupdate").val(namakapal);
     
     }); 
});
</script>

<!-- list container -->
<script type="text/javascript">
  $(document).ready(function(){
    $('#listcontainer').on('change', function() {
       if ($(this).val() !=" ") 
      {
       $("#namacontainer").attr("readonly", "readonly");

       // just hide prevent human error
       $("#inputcontainer").attr("disabled","disabled");
       $("#inputcontainer").val("");
       $("#namakapal").attr("readonly","readonly");
      } 
      else 
      {
       $("#namacontainer").removeAttr("readonly");

       // just hide prevent human error
       $("#inputcontainer").removeAttr("disabled");
       $("#inputcontainer").val("");
       $("#namakapal").removeAttr("readonly","readonly");

      }

      var idconttemp = $(this).find(":selected").data('idcontainertemp');
      var e = document.getElementById("listcontainer");
      var namacontainer = e.options[e.selectedIndex].value;
      var sisacontainer = $(this).find(':selected').data('sisacontainer');
      var batasminimjeniscont = $(this).find(':selected').data('jeniscontainer');
      var tgllistcontainer = $(this).find(':selected').data('tgllistcontainer');
      // selected nama kapal sesuai dengan container terpilih
      var namakapalselect = $(this).find(':selected').data('namakapalselectedcontainer');
      

      // id jenis container temp
      $("#idjenisconttemp").val(idconttemp);
      // nama container
      $("#namacontainer").val(namacontainer);
      // sett view sisa isi container yang sudah di gunakan sesuai dengan DB temp
      $("#sisatampil").val(sisacontainer);
      // sett view hidden untuk pengurangan yang terpilih dari list container
      $("#sisahide").val(sisacontainer);
      // for set batas maks sesuai dengan jenis container di DB temp
      $("#btsminimtampil").val(batasminimjeniscont);
      // set tgl container yg bersangutan
      $(".datetglkapal").val(tgllistcontainer);
      // set dropdown nama kapal
      // var e = document.getElementById("listkapal");
      // e.options[e.selectedIndex].value =namakapalselect;
      // $("#namakapal").val(namakapalselect);
     }); 
});
</script>

<script type="text/javascript">
	$(document).ready(function(){
    $('#inputcontainer').on('change', function() {
		 var id = $(this).find(':selected').data('id');
		 var minim = $(this).find(':selected').data('minimum');
     // var updateminim = $(this).find(':selected').data('updateminim');

		 $("#batasminimhide").val(minim);
		 $("#btsminimtampil").val(minim);
     // $("#sisahide").val(updateminim);
      $("#sisatampil").val(minim);
     
    }); 
});
</script>

<script type="text/javascript">
  function validate(form)
  {
    if(confirm("Tambah Data Pengiriman ke Packing List?"))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
</script>






<!-- modal edit pengiriman -->
<script type="text/javascript">
	  $('.editpengiriman').on('click',function(e){
  	  	var idpengiriman = $(this).data('idpengiriman');
        var idbarang = $(this).data('idbarang');
        var idpengirim = $(this).data('idpengirim');
        var idpenerima = $(this).data('idpenerima');
        var kapal = $(this).data('kapal');
        var tanggalpengiriman = $(this).data('tglpengiriman');
        var container = $(this).data('container');
        var pengirim = $(this).data('pengirim');
        var penerima = $(this).data('penerima');
        var panjang = $(this).data('panjang');
        var lebar = $(this).data('lebar');
        var tinggi = $(this).data('tinggi');
        var totalkubikasi = $(this).data('totalkubikasi');
        var barang = $(this).data('barang');
        var qty = $(this).data('qty');
        var sisacontainer =$(this).data('sisacontainer');

	      $("#idpengirimanupdate").val(idpengiriman);
        $("#idbarangupdate").val(idbarang);
        $("#idpengirimupdate").val(idpengirim);
        $("#idpenerimaupdate").val(idpenerima);
        $("#kapalupdate").val(kapal);
        $("#datepickereditpengiriman").val(tanggalpengiriman);
        $("#containerupdate").val(container);
        $("#pengirimupdate").val(pengirim);
        $("#penerimaupdate").val(penerima);
        $("#panjangupdate").val(panjang);
        $("#lebarupdate").val(lebar);
        $("#tinggiupdate").val(tinggi);
        $("#totalkubikasiupdate").val(totalkubikasi);
        $("#barangupdate").val(barang); 
        $("#qtyupdate").val(qty); 
        $("#sisatampiledit").val(sisacontainer);
        $("#sisatampiledithide").val(sisacontainer);
        $("#totalkubikasiupdatehide").val(totalkubikasi);
	 
   });
</script>
<script type="text/javascript">
  $("#updatekapaledit").on("change", function(e){
    if ($(this).val() !=" ") 
      {
       $("#kapalupdate").attr("readonly", "readonly");
     } 
      else 
      {
       $("#kapalupdate").removeAttr("readonly");
      }

      var e = document.getElementById("updatekapaledit");
      var namakapal = e.options[e.selectedIndex].value;
     
      $("#kapalupdate").val(namakapal);

  });
</script>
<script>
  $(document).on('click','.update',function(){
    var idpengiriman = $('#idpengirimanupdate').val();
    var form = new FormData($('#updatepengirimanmodal')[0]);
    var url = '{{route("pengiriman.update",":id")}}';
    url = url.replace(':id',idpengiriman);
 	//$('.loadingpengiriman').show();
 	if (confirm("Ingin Update Data?")) {

 	  $.ajax({
      url: url,
      type:'POST',
      data:form,
      async: false,
      cache: false,
      contentType: false,
      enctype: 'multipart/form-data',
      processData: false,
      }).done(function(response){
      	alert("Data Berhasil Diperbarui");
      	location.reload();	
      	 //$('.loadingpengiriman').hide();
     });	
 	}
 	else
 	{
 		return false;
 	}  
  });
</script>
<script type="text/javascript">
	
$(document).on('input', '.change', function()
{
   var qty                     = $('#qtyupdate').val();
   var panjang                 = $('#panjangupdate').val();
   var lebar                   = $('#lebarupdate').val();
   var tinggi                  = $('#tinggiupdate').val();
   var sisacontainerhide       = parseFloat($('#sisatampiledithide').val());
   var totalkubikasiupdatehide = parseFloat($('#totalkubikasiupdatehide').val());
   var volume                  = qty*(panjang*lebar*tinggi);

   var decimal = parseFloat(volume.toFixed(2));
   // if decimal (total kubikasi yang ditampilkan kurang dari total kubikasi yang ada di database barang)
   if (decimal<totalkubikasiupdatehide) 
   {
    //yang dikurangkan adalah total kubikasi per item yang ada di DB BARANG dengan decimal (total kubikasi dari yang mau diedit)
    var pengurangan = totalkubikasiupdatehide-decimal;
    sisacontainerhide += pengurangan;
    // yang ditampilkan yang hide agar jadi acuan (tidak bertambah terus)
    $('#sisatampiledit').val(sisacontainerhide);
   }
   else if(decimal==totalkubikasiupdatehide)
   {
       // yang ditampilkan yang hide agar jadi acuan (tidak bertambah terus)
    $('#sisatampiledit').val(sisacontainerhide);
   }
   else
   {
    //yang dikurangkan adalah total kubikasi decimal (total kubikasi dari yang mau diedit)  dengan total kubikasi per item yang ada di DB BARANG
     var pengurangan1 = decimal-totalkubikasiupdatehide;
     sisacontainerhide -= pengurangan1;
        // yang ditampilkan yang hide agar jadi acuan (tidak bertambah terus)
     $('#sisatampiledit').val(sisacontainerhide);
   }
  

   $('#totalkubikasiupdate').val(decimal);



});

</script>
<!-- end modal edit pengiriman -->

<!-- delete pengiriman -->
<script type="text/javascript">
	  $('.deletepengiriman').on('click',function(e){
    var idkirimdelete = $(this).data("idpengirimandelete");
    var idbrgdelete = $(this).data("idbarangdelete");
    var totalkubikasibarang = parseFloat($(this).data("totalkubikasi"));

    // sisa container untuk update nambah barang setelah didelete
    var sisacontainerbarang = parseFloat($(this).data("sisacontainer"));

    // namacontainer untuk where delete
    var namacontainerdelete = $(this).data("container");

    // penjumlahan after delete
    var sisacontainerafterdelete = totalkubikasibarang + sisacontainerbarang;

    $("#idpengirimandeletehide").val(idkirimdelete);
    $("#idbarangdeletehide").val(idbrgdelete);
    // $("#idpengirimdeletehide").val(idpengirimdelete);
    // $("#idpenerimadeletehide").val(idpenerimadelete);
    $("#namacontaineruntukdelete").val(namacontainerdelete);

    $("#sisacontainertemp").val(sisacontainerafterdelete);

	  var form = new FormData($('#formdeletepengiriman')[0]);
	  var url = '{{route("pengiriman.deletelistpengiriman")}}';
	 	if (confirm("Ingin Hapus Data?")) {

	 	  $.ajax({
	      url: url,
	      type:'POST',
	      data:form,
	      async: false,
	      cache: false,
	      contentType: false,
	      enctype: 'multipart/form-data',
	      processData: false,
	      }).done(function(response){
	      	alert("Data Berhasil Dihapus");
	      	location.reload();	
	     });	
	 	}
	 	else
	 	{
	 		return false;
	 	}  
   });
</script>


<script>

  $(function () {
    $('#tablebarangpengiriman').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  });

</script>

 @endsection

